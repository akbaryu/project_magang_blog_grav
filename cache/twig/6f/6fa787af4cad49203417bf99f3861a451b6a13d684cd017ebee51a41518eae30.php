<?php

/* @Page:C:/xampp/htdocs/intromagang/grav/user/pages/02.blog/02.flat_file_struktur */
class __TwigTemplate_2e65f51251b0a42cdea02c6cfaca569d9ef69b8c9d7d3a5e3701c3eeb4ec82aa extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<p>Flat-file Structure</p>
<p>One of Grav's biggest features is its flat-file structure. This enables it to not only break free of the need of a database, but for its content to be simply created, swapped, and edited.</p>
<p>Installing Grav on a server is as simple as downloading the Grav core, or a skeleton which includes demo content to get you started, and unpacking it on your server.</p>
<p>There is no database to set up or installation wizard to work through.
<img src=\"/intromagang/grav/user/pages/02.blog/02.flat_file_struktur/S8d0ZL8.png\" /></p>
<p>User Folder</p>
<p>Grav has an intuitive organizational structure, as well. For example, just about everything that isn't core to Grav is stored in a user folder. This includes any custom configuration and settings, user accounts, themes, site-specific assets, plugins, and content.</p>
<p>At its basic level, backing up a Grav site is as simple as making a copy of the user folder.</p>
<p>The site's content is stored in the user/pages directory. Here, pages are organized using a directory structure. Each blog post, for example, would have its own folder containing a markdown file and any images used in the post. Images can be stored in many different ways, but this is one popular example.<img src=\"/intromagang/grav/user/pages/02.blog/02.flat_file_struktur/7x9LS8a.png\" /></p>";
    }

    public function getTemplateName()
    {
        return "@Page:C:/xampp/htdocs/intromagang/grav/user/pages/02.blog/02.flat_file_struktur";
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }
}
/* <p>Flat-file Structure</p>*/
/* <p>One of Grav's biggest features is its flat-file structure. This enables it to not only break free of the need of a database, but for its content to be simply created, swapped, and edited.</p>*/
/* <p>Installing Grav on a server is as simple as downloading the Grav core, or a skeleton which includes demo content to get you started, and unpacking it on your server.</p>*/
/* <p>There is no database to set up or installation wizard to work through.*/
/* <img src="/intromagang/grav/user/pages/02.blog/02.flat_file_struktur/S8d0ZL8.png" /></p>*/
/* <p>User Folder</p>*/
/* <p>Grav has an intuitive organizational structure, as well. For example, just about everything that isn't core to Grav is stored in a user folder. This includes any custom configuration and settings, user accounts, themes, site-specific assets, plugins, and content.</p>*/
/* <p>At its basic level, backing up a Grav site is as simple as making a copy of the user folder.</p>*/
/* <p>The site's content is stored in the user/pages directory. Here, pages are organized using a directory structure. Each blog post, for example, would have its own folder containing a markdown file and any images used in the post. Images can be stored in many different ways, but this is one popular example.<img src="/intromagang/grav/user/pages/02.blog/02.flat_file_struktur/7x9LS8a.png" /></p>*/
