<?php

/* @Page:C:/xampp/htdocs/intromagang/grav/user/pages/02.blog/03.why_grav */
class __TwigTemplate_ce35998998aed208fa75fafafb1c1f12f632fb5d33aeef5abc4a42bf81457eb0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<p>The inspiration behind Grav's development was to create a CMS that fills a gap between simple, static sites and complex multiuser blogs. When the project's founder, Andy Miller, took a look at the flat-file solutions already out there, he found current options were either commercial, underpowered, or both.</p>
<p>This isn't for lack of technology. Twig tempting, YAML configuration, markdown content support, and other exceptionally user-friendly features were out there, but they hadn't been assembled into a single CMS available for free.</p>
<p>We felt it was important to make Grav not only free, but open source. This enables its community of developers to contribute code, have a definite say in the project's development, and to easily create plugins and themes that take full advantage of Grav's capabilities.</p>";
    }

    public function getTemplateName()
    {
        return "@Page:C:/xampp/htdocs/intromagang/grav/user/pages/02.blog/03.why_grav";
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }
}
/* <p>The inspiration behind Grav's development was to create a CMS that fills a gap between simple, static sites and complex multiuser blogs. When the project's founder, Andy Miller, took a look at the flat-file solutions already out there, he found current options were either commercial, underpowered, or both.</p>*/
/* <p>This isn't for lack of technology. Twig tempting, YAML configuration, markdown content support, and other exceptionally user-friendly features were out there, but they hadn't been assembled into a single CMS available for free.</p>*/
/* <p>We felt it was important to make Grav not only free, but open source. This enables its community of developers to contribute code, have a definite say in the project's development, and to easily create plugins and themes that take full advantage of Grav's capabilities.</p>*/
