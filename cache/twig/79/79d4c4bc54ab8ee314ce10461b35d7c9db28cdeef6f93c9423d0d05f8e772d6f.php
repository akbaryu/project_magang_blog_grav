<?php

/* partials/page_head.html.twig */
class __TwigTemplate_924eb2a87b325f1df5d94dab34df82805e17fc23ce6ed711529758c5026c37d9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@nucleus/page_head.html.twig", "partials/page_head.html.twig", 1);
        $this->blocks = array(
            'head_title' => array($this, 'block_head_title'),
            'head_application' => array($this, 'block_head_application'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@nucleus/page_head.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_head_title($context, array $blocks = array())
    {
        // line 4
        echo "    <meta charset=\"utf-8\" />
    <title>";
        // line 5
        if ($this->getAttribute((isset($context["header"]) ? $context["header"] : null), "title", array())) {
            echo $this->getAttribute((isset($context["header"]) ? $context["header"] : null), "title", array());
            echo " | ";
        }
        echo $this->getAttribute((isset($context["site"]) ? $context["site"] : null), "title", array());
        echo "</title>
    ";
        // line 6
        if ($this->getAttribute((isset($context["header"]) ? $context["header"] : null), "description", array())) {
            // line 7
            echo "        <meta name=\"description\" content=\"";
            echo $this->getAttribute((isset($context["header"]) ? $context["header"] : null), "description", array());
            echo "\">
    ";
        } else {
            // line 9
            echo "        <meta name=\"description\" content=\"";
            echo $this->getAttribute((isset($context["site"]) ? $context["site"] : null), "description", array());
            echo "\">
    ";
        }
        // line 11
        echo "    ";
        if ($this->getAttribute((isset($context["header"]) ? $context["header"] : null), "robots", array())) {
            // line 12
            echo "        <meta name=\"robots\" content=\"";
            echo $this->getAttribute((isset($context["header"]) ? $context["header"] : null), "robots", array());
            echo "\">
    ";
        }
    }

    // line 16
    public function block_head_application($context, array $blocks = array())
    {
        // line 17
        echo "    ";
        echo $this->getAttribute($this->getAttribute((isset($context["gantry"]) ? $context["gantry"] : null), "platform", array()), "finalize", array());
        echo "
    ";
        // line 18
        echo $this->getAttribute((isset($context["assets"]) ? $context["assets"] : null), "css", array(), "method");
        echo "

    ";
        // line 20
        echo $this->getAttribute((isset($context["assets"]) ? $context["assets"] : null), "js", array(), "method");
        echo "
";
    }

    public function getTemplateName()
    {
        return "partials/page_head.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  81 => 20,  76 => 18,  71 => 17,  68 => 16,  60 => 12,  57 => 11,  51 => 9,  45 => 7,  43 => 6,  35 => 5,  32 => 4,  29 => 3,  11 => 1,);
    }
}
/* {% extends "@nucleus/page_head.html.twig" %}*/
/* */
/* {% block head_title %}*/
/*     <meta charset="utf-8" />*/
/*     <title>{% if header.title %}{{ header.title }} | {% endif %}{{ site.title }}</title>*/
/*     {% if header.description %}*/
/*         <meta name="description" content="{{ header.description }}">*/
/*     {% else %}*/
/*         <meta name="description" content="{{ site.description }}">*/
/*     {% endif %}*/
/*     {% if header.robots %}*/
/*         <meta name="robots" content="{{ header.robots }}">*/
/*     {% endif %}*/
/* {% endblock %}*/
/* */
/* {% block head_application %}*/
/*     {{ gantry.platform.finalize }}*/
/*     {{ assets.css() }}*/
/* */
/*     {{ assets.js() }}*/
/* {% endblock %}*/
/* */
