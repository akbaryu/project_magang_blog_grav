<?php

/* @Page:C:/xampp/htdocs/intromagang/grav/user/pages/02.blog/01.belajar-git */
class __TwigTemplate_1e32a96d6e53679dc8da2e51163e3d7ec8805ec975770b9d651e96f2fb7a5930 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<p>Git saat ini semakin marak digunakan di dunia pengembangan perangkat lunak, termasuk pengembangan website. Git bisa membantu anda untuk mengontrol versi wesite anda, dengan git anda bisa membuat perubahan pada sistem yang anda buat dan ketika perubahan tersebut nemimbulkan kekacauan maka anda bisa kembali ke versi semula yang lebih stabil.</p>
<p>Belajar git tidak lah mudah, untuk yang baru memulai tentu akan sangat bingung apa dan bagaimana sebenarnya git ini. Namun anda tidak perlu khawatir, karena banyak tutorial tentang git ini, termasuk tutorial interaktif agar anda semakin mudah untuk memahami penggunaan dan fungsi git.</p>
<p>Berikut ini adalah daftar tutorial interaktif gratis yang bisa anda ikuti sebagai panduan anda belajar git, tutorial nya sudah diatur berdasarkan tingkat kesulitan, jadi jurnal web menyarankan anda untuk belajar dari tingkat dasar dulu sehingga anda bisa lebih paham.!</p>
<h2>1. Try Git</h2>
<p><a href=\"/intromagang/grav/blog/belajar-git/try-git.png\"></a></p>
<p><strong>Try git</strong> adalah tutorial interaktif sebagai tahap awal untuk anda yang baru pertama kali belajar git (pemula). Tutorial ini berisi beberapa perintah (command) GIT yang sering digunakan, seperti membuat Repository, melakukan perubahan dan bekerja remote.</p>
<p>Kursus kilat ini akan memakan waktu setidak nya 15 menit.</p>
<h2>2. Git Real</h2>
<p>Git Real adalah kursus online interaktif di Code School. Disini tersedia instruksi berupa video dan tersedia pula tantangan-tantangan agar anda lebih mahir.</p>
<p>Namun sayang nya hanya tingkat pertama yang gratis, sisanya anda mesti mengeluarkan kocek anda untuk belajar, hm…untuk sebuah pengetahuan harusnya ini tidak masalah untuk anda :)</p>
<p>Yang menarik di tutorial ini adalah mereka memfokuskan pada kasus-kasus git yang berhubungan dengan pengembangan web.</p>
<p>Baca Juga: Tutorial GIT : Instalasi, Penggunaan &amp; Upload Proyek ke Github</p>
<h2>3. Learn Git Branching</h2>
<p>Mungkin konsep git yang paling sulit itu tentang Source Tree, source-tree traversal dan brancing. Namun di website interaktif ini anda akan belajar itu semua.</p>
<p>Disini untuk mempelajari Git Branching dibagi menjadi lima bagian dengan tingkat kesulitan yang berbeda, dan setiap bagian memiliki 2-5 modul didalamnya. Silahkan kunjungi Learn Git Branching</p>
<p>Itulah tiga website interaktif yang bisa anda kunjungi untuk belajar Git secara online, semoga artikel ini berguna untuk anda semua yang sedang belajar Version Control untuk aplikasi yang anda buat.</p>";
    }

    public function getTemplateName()
    {
        return "@Page:C:/xampp/htdocs/intromagang/grav/user/pages/02.blog/01.belajar-git";
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }
}
/* <p>Git saat ini semakin marak digunakan di dunia pengembangan perangkat lunak, termasuk pengembangan website. Git bisa membantu anda untuk mengontrol versi wesite anda, dengan git anda bisa membuat perubahan pada sistem yang anda buat dan ketika perubahan tersebut nemimbulkan kekacauan maka anda bisa kembali ke versi semula yang lebih stabil.</p>*/
/* <p>Belajar git tidak lah mudah, untuk yang baru memulai tentu akan sangat bingung apa dan bagaimana sebenarnya git ini. Namun anda tidak perlu khawatir, karena banyak tutorial tentang git ini, termasuk tutorial interaktif agar anda semakin mudah untuk memahami penggunaan dan fungsi git.</p>*/
/* <p>Berikut ini adalah daftar tutorial interaktif gratis yang bisa anda ikuti sebagai panduan anda belajar git, tutorial nya sudah diatur berdasarkan tingkat kesulitan, jadi jurnal web menyarankan anda untuk belajar dari tingkat dasar dulu sehingga anda bisa lebih paham.!</p>*/
/* <h2>1. Try Git</h2>*/
/* <p><a href="/intromagang/grav/blog/belajar-git/try-git.png"></a></p>*/
/* <p><strong>Try git</strong> adalah tutorial interaktif sebagai tahap awal untuk anda yang baru pertama kali belajar git (pemula). Tutorial ini berisi beberapa perintah (command) GIT yang sering digunakan, seperti membuat Repository, melakukan perubahan dan bekerja remote.</p>*/
/* <p>Kursus kilat ini akan memakan waktu setidak nya 15 menit.</p>*/
/* <h2>2. Git Real</h2>*/
/* <p>Git Real adalah kursus online interaktif di Code School. Disini tersedia instruksi berupa video dan tersedia pula tantangan-tantangan agar anda lebih mahir.</p>*/
/* <p>Namun sayang nya hanya tingkat pertama yang gratis, sisanya anda mesti mengeluarkan kocek anda untuk belajar, hm…untuk sebuah pengetahuan harusnya ini tidak masalah untuk anda :)</p>*/
/* <p>Yang menarik di tutorial ini adalah mereka memfokuskan pada kasus-kasus git yang berhubungan dengan pengembangan web.</p>*/
/* <p>Baca Juga: Tutorial GIT : Instalasi, Penggunaan &amp; Upload Proyek ke Github</p>*/
/* <h2>3. Learn Git Branching</h2>*/
/* <p>Mungkin konsep git yang paling sulit itu tentang Source Tree, source-tree traversal dan brancing. Namun di website interaktif ini anda akan belajar itu semua.</p>*/
/* <p>Disini untuk mempelajari Git Branching dibagi menjadi lima bagian dengan tingkat kesulitan yang berbeda, dan setiap bagian memiliki 2-5 modul didalamnya. Silahkan kunjungi Learn Git Branching</p>*/
/* <p>Itulah tiga website interaktif yang bisa anda kunjungi untuk belajar Git secara online, semoga artikel ini berguna untuk anda semua yang sedang belajar Version Control untuk aplikasi yang anda buat.</p>*/
