<?php

/* partials/blog_item.html.twig */
class __TwigTemplate_a4e05f091fcff497a78942da5ba2ab4d4fab62f71b19a43b989f416fbed17c71 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<section class=\"blog-item\">
    ";
        // line 2
        $context["banner"] = twig_first($this->env, $this->getAttribute($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "assets", array()), "images", array()));
        // line 3
        echo "    ";
        if ((isset($context["banner"]) ? $context["banner"] : null)) {
            // line 4
            echo "    <div class=\"blog-header\">
        ";
            // line 5
            echo $this->getAttribute($this->getAttribute((isset($context["banner"]) ? $context["banner"] : null), "cropZoom", array(0 => 900, 1 => 300), "method"), "html", array());
            echo "
        <h1><a href=\"";
            // line 6
            echo $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "url", array());
            echo "\">";
            echo $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "title", array());
            echo "</a></h1>
    </div>
    ";
        } else {
            // line 9
            echo "    <div class=\"blog-title\">
        <h1><a href=\"";
            // line 10
            echo $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "url", array());
            echo "\">";
            echo $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "title", array());
            echo "</a></h1>
    </div>
    ";
        }
        // line 13
        echo "
    <div class=\"blog-details\">
        <span class=\"blog-date\">Posted: ";
        // line 15
        echo twig_date_format_filter($this->env, $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "date", array()));
        echo "</span>

        ";
        // line 17
        if ($this->getAttribute($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "taxonomy", array()), "tag", array())) {
            // line 18
            echo "        <span class=\"tags\">
            ";
            // line 19
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "taxonomy", array()), "tag", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["tag"]) {
                // line 20
                echo "            <a href=\"";
                echo (isset($context["base_url_relative"]) ? $context["base_url_relative"] : null);
                echo (isset($context["list_url"]) ? $context["list_url"] : null);
                echo "/tag:";
                echo $context["tag"];
                echo "\">";
                echo $context["tag"];
                echo "</a>
            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['tag'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 22
            echo "        </span>
        ";
        }
        // line 24
        echo "    </div>

    <div class=\"blog-padding\">
    ";
        // line 27
        if ((isset($context["truncate"]) ? $context["truncate"] : null)) {
            // line 28
            echo "    <p>";
            echo call_user_func_array($this->env->getFilter('truncate')->getCallable(), array($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "content", array()), 550));
            echo "</p>
    <p><a class=\"button\" href=\"";
            // line 29
            echo $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "url", array());
            echo "\">Continue: ";
            echo $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "title", array());
            echo "</a></p>
    ";
        } else {
            // line 31
            echo "    <p>";
            echo $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "content", array());
            echo "</p>
    ";
        }
        // line 33
        echo "    </div>

    ";
        // line 35
        if ( !(isset($context["truncate"]) ? $context["truncate"] : null)) {
            // line 36
            echo "    <div class=\"blog-images\">
    ";
            // line 37
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "assets", array()), "all", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["asset"]) {
                // line 38
                echo "        ";
                echo $this->getAttribute($this->getAttribute($this->getAttribute($context["asset"], "lightbox", array()), "cropResize", array(0 => 1000, 1 => 100), "method"), "html", array());
                echo "
    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['asset'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 40
            echo "    </div>
    ";
        }
        // line 42
        echo "
</section>
";
    }

    public function getTemplateName()
    {
        return "partials/blog_item.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  140 => 42,  136 => 40,  127 => 38,  123 => 37,  120 => 36,  118 => 35,  114 => 33,  108 => 31,  101 => 29,  96 => 28,  94 => 27,  89 => 24,  85 => 22,  71 => 20,  67 => 19,  64 => 18,  62 => 17,  57 => 15,  53 => 13,  45 => 10,  42 => 9,  34 => 6,  30 => 5,  27 => 4,  24 => 3,  22 => 2,  19 => 1,);
    }
}
/* <section class="blog-item">*/
/*     {% set banner = page.assets.images|first %}*/
/*     {% if banner %}*/
/*     <div class="blog-header">*/
/*         {{ banner.cropZoom(900,300).html }}*/
/*         <h1><a href="{{ page.url }}">{{ page.title }}</a></h1>*/
/*     </div>*/
/*     {% else %}*/
/*     <div class="blog-title">*/
/*         <h1><a href="{{ page.url }}">{{ page.title }}</a></h1>*/
/*     </div>*/
/*     {% endif %}*/
/* */
/*     <div class="blog-details">*/
/*         <span class="blog-date">Posted: {{ page.date|date() }}</span>*/
/* */
/*         {% if page.taxonomy.tag %}*/
/*         <span class="tags">*/
/*             {% for tag in page.taxonomy.tag %}*/
/*             <a href="{{ base_url_relative }}{{ list_url }}/tag:{{ tag }}">{{ tag }}</a>*/
/*             {% endfor %}*/
/*         </span>*/
/*         {% endif %}*/
/*     </div>*/
/* */
/*     <div class="blog-padding">*/
/*     {% if truncate %}*/
/*     <p>{{ page.content|truncate(550) }}</p>*/
/*     <p><a class="button" href="{{ page.url }}">Continue: {{ page.title }}</a></p>*/
/*     {% else %}*/
/*     <p>{{ page.content }}</p>*/
/*     {% endif %}*/
/*     </div>*/
/* */
/*     {% if not truncate %}*/
/*     <div class="blog-images">*/
/*     {% for asset in page.assets.all %}*/
/*         {{ asset.lightbox.cropResize(1000,100).html }}*/
/*     {% endfor %}*/
/*     </div>*/
/*     {% endif %}*/
/* */
/* </section>*/
/* */
