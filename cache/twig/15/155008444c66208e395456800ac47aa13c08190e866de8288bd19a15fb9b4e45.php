<?php

/* @Page:C:/xampp/htdocs/intromagang/grav/user/pages/02.blog/05.apa-itu-internet-of-things-iot */
class __TwigTemplate_c4b8fed812a58116a373b97e3019702edd882ce8d5cffaea06ab40a353d20508 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<p>Apa itu Internet of Things? Internet of Things adalah segala sesuatu yang terkoneksi dan terhubung dengan Internet. Benda-benda tersebut saling terkoneksi satu sama lain dengan teknologi Internet</p>
<p>Pada hal ini, biasanya sesuatu ini mencakupi benda yang bisa dimanfaatkan dengan menggunakan teknologi Internet, seperti misalnya elektronik (kulkas, lampu, pendingin ruangan), ataupun dapat pula diintegrasikan dengan makhluk hidup (hewan)</p>
<h3>Sejarah Internet of Things</h3>
<p>Perkembangan Internet of Things sendiri semakin sini semakin pesat, yang disebabkan oleh teknologi Internet yang semakin cepat dan perangkat keras yang semakin cepat dan efisien dari segi daya</p>
<h3>Contoh Internet of Things</h3>
<p>Sebagai contoh, teknologi wearable yang bisa temukan adalah smartwatch, yang biasanya kita temukan di Android Wear, Apple Watch ataupun Pebble.</p>
<p>Smart-band seperti Xiaomi Mi Band dan FitBit sendiri pun memiliki manfaat yang sama, namun difokuskan kepada solusi di bidang kesehatan (fitness)</p>
<p><img src=\"/intromagang/grav/user/pages/02.blog/05.apa-itu-internet-of-things-iot/1.jpg\" /></p>
<h3>Apa itu Nest</h3>
<p>Ada pula solusi Internet of Things yang mencakup solusi di dalam rumah, yaitu Home Automation seperti Nest. Nest sendiri merupakan IoT yang mencakup tentang pendeteksian suhu (Thermostat) dan sistem keamanan (Security System)</p>
<p><img src=\"/intromagang/grav/user/pages/02.blog/05.apa-itu-internet-of-things-iot/2.jpg\" /></p>
<p>(red)</p>
<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/Q3ur8wzzhBU\" frameborder=\"0\" allowfullscreen></iframe>";
    }

    public function getTemplateName()
    {
        return "@Page:C:/xampp/htdocs/intromagang/grav/user/pages/02.blog/05.apa-itu-internet-of-things-iot";
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }
}
/* <p>Apa itu Internet of Things? Internet of Things adalah segala sesuatu yang terkoneksi dan terhubung dengan Internet. Benda-benda tersebut saling terkoneksi satu sama lain dengan teknologi Internet</p>*/
/* <p>Pada hal ini, biasanya sesuatu ini mencakupi benda yang bisa dimanfaatkan dengan menggunakan teknologi Internet, seperti misalnya elektronik (kulkas, lampu, pendingin ruangan), ataupun dapat pula diintegrasikan dengan makhluk hidup (hewan)</p>*/
/* <h3>Sejarah Internet of Things</h3>*/
/* <p>Perkembangan Internet of Things sendiri semakin sini semakin pesat, yang disebabkan oleh teknologi Internet yang semakin cepat dan perangkat keras yang semakin cepat dan efisien dari segi daya</p>*/
/* <h3>Contoh Internet of Things</h3>*/
/* <p>Sebagai contoh, teknologi wearable yang bisa temukan adalah smartwatch, yang biasanya kita temukan di Android Wear, Apple Watch ataupun Pebble.</p>*/
/* <p>Smart-band seperti Xiaomi Mi Band dan FitBit sendiri pun memiliki manfaat yang sama, namun difokuskan kepada solusi di bidang kesehatan (fitness)</p>*/
/* <p><img src="/intromagang/grav/user/pages/02.blog/05.apa-itu-internet-of-things-iot/1.jpg" /></p>*/
/* <h3>Apa itu Nest</h3>*/
/* <p>Ada pula solusi Internet of Things yang mencakup solusi di dalam rumah, yaitu Home Automation seperti Nest. Nest sendiri merupakan IoT yang mencakup tentang pendeteksian suhu (Thermostat) dan sistem keamanan (Security System)</p>*/
/* <p><img src="/intromagang/grav/user/pages/02.blog/05.apa-itu-internet-of-things-iot/2.jpg" /></p>*/
/* <p>(red)</p>*/
/* <iframe width="560" height="315" src="https://www.youtube.com/embed/Q3ur8wzzhBU" frameborder="0" allowfullscreen></iframe>*/
