<?php

/* @Page:C:/xampp/htdocs/intromagang/grav/user/pages/02.blog/01.posting-pertama */
class __TwigTemplate_763bfaa118d44963c6d87c08ff7d2ae31cff307a1be821af10390481261d7f42 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<p>Grav adalah CMS populer yang dapat digunakan untuk dengan cepat membuat blog atau website penuh, baik dari awal atau menggunakan beberapa template premade mereka sangat berguna. Jadi apa yang berbeda tentang Grav dibandingkan dengan perangkat lunak seperti WordPress untuk menjalankan sebuah blog? Perbedaan utama adalah bahwa hal itu dapat digunakan untuk membuat website flat file atau file blog datar, ini berarti tidak ada database yang sedang digunakan dengan situs Anda.</p>
<p><img src=\"/intromagang/grav/user/pages/02.blog/01.posting-pertama/grav-cms-blog-screen.png\" /></p>
<h3>Menambahkan Tulisan ke Anda Grav CMS Blog</h3>
<p>Sekarang Anda bisa mendapatkan untuk mengedit posting di situs Anda. Hal ini dilakukan dari dalam folder pengguna dan dalam itu, halaman folder. Path lengkap dari direktori default Anda harus sesuatu seperti ini:</p>
<p>user -&gt; halaman -&gt; 01.blog</p>
<p>Dalam folder 01.blog Anda akan melihat bahwa setiap artikel memiliki itu folder sendiri, dan bahwa nama folder akan menjadi permalink digunakan dalam URL situs mengakses artikel, sehingga setara dengan siput URL di WordPress. Ini berarti bahwa semua folder idealnya harus seluruhnya ditulis dalam huruf kecil dan dengan strip bukannya ruang.</p>
<p>Dalam setiap folder Item blog akan ada file penurunan harga bernama, item.md. Setiap kali Anda menambahkan artikel baru Anda harus membuat nama file item.md untuk memiliki konten artikel ditambahkan dalam. gambar opsional juga dapat dimasukkan jika Anda ingin fitur gambar dengan posting Anda.</p>";
    }

    public function getTemplateName()
    {
        return "@Page:C:/xampp/htdocs/intromagang/grav/user/pages/02.blog/01.posting-pertama";
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }
}
/* <p>Grav adalah CMS populer yang dapat digunakan untuk dengan cepat membuat blog atau website penuh, baik dari awal atau menggunakan beberapa template premade mereka sangat berguna. Jadi apa yang berbeda tentang Grav dibandingkan dengan perangkat lunak seperti WordPress untuk menjalankan sebuah blog? Perbedaan utama adalah bahwa hal itu dapat digunakan untuk membuat website flat file atau file blog datar, ini berarti tidak ada database yang sedang digunakan dengan situs Anda.</p>*/
/* <p><img src="/intromagang/grav/user/pages/02.blog/01.posting-pertama/grav-cms-blog-screen.png" /></p>*/
/* <h3>Menambahkan Tulisan ke Anda Grav CMS Blog</h3>*/
/* <p>Sekarang Anda bisa mendapatkan untuk mengedit posting di situs Anda. Hal ini dilakukan dari dalam folder pengguna dan dalam itu, halaman folder. Path lengkap dari direktori default Anda harus sesuatu seperti ini:</p>*/
/* <p>user -&gt; halaman -&gt; 01.blog</p>*/
/* <p>Dalam folder 01.blog Anda akan melihat bahwa setiap artikel memiliki itu folder sendiri, dan bahwa nama folder akan menjadi permalink digunakan dalam URL situs mengakses artikel, sehingga setara dengan siput URL di WordPress. Ini berarti bahwa semua folder idealnya harus seluruhnya ditulis dalam huruf kecil dan dengan strip bukannya ruang.</p>*/
/* <p>Dalam setiap folder Item blog akan ada file penurunan harga bernama, item.md. Setiap kali Anda menambahkan artikel baru Anda harus membuat nama file item.md untuk memiliki konten artikel ditambahkan dalam. gambar opsional juga dapat dimasukkan jika Anda ingin fitur gambar dengan posting Anda.</p>*/
