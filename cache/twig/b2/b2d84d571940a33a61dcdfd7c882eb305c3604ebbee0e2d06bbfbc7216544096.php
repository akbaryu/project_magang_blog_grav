<?php

/* partials/nav-toggle.html.twig */
class __TwigTemplate_4c8ba61764158fda7a3b8e1a7c235ef6578422ec348a6327a9ee901e469061dc extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<button class=\"lines-button x\" type=\"button\" role=\"button\" aria-label=\"Toggle Navigation\" data-sidebar-mobile-toggle>
    <span class=\"lines\"></span>
</button>
";
    }

    public function getTemplateName()
    {
        return "partials/nav-toggle.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }
}
/* <button class="lines-button x" type="button" role="button" aria-label="Toggle Navigation" data-sidebar-mobile-toggle>*/
/*     <span class="lines"></span>*/
/* </button>*/
/* */
