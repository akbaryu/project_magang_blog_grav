<?php

/* @Page:C:/xampp/htdocs/intromagang/grav/user/pages/02.blog/04.perbedaan-css-position-static-relative-absolute-fixed-dan-inherit */
class __TwigTemplate_3a28157603e2c56230b598e5a84b647af7d13e263da8582f577cfcb75b9b7211 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<p>Kali ini saya akan membahas apa sih perbedaan antara CSS Position Static, Relative, Absolute, Fixed dan Inherit.</p>
<p>Perlu diketahui sebelumnya, bahwa properti position memanipulasi lokasi elemen. Ada beberapa jenis position yang bisa kita tentukan, antara lain :</p>
<ul>
<li>Static</li>
<li>Relative</li>
<li>Absolute</li>
<li>Fixed</li>
<li>Inherit</li>
</ul>
<h3>Static</h3>
<p>Properti static merupakan properti bawaan semua elemen, dimana semua elemen akan memiliki properti ini. Dengan properti ini, elemen akan menempati posisi mengikuti alur halaman.</p>
<p>Jadi, apabila kita menambahkan value top, bottom, left, right, value tersebut tidak akan menghasilkan apapun.</p>
<h3>Relative</h3>
<p>Posisi elemen tetap mengikuti alur dokumen, hampir sama dengan value static. Yang menjadi perbedaan yaitu, value top, bottom, left, right akan berfungsi. Pengaturan posisi pada suatu elemen menggunakan value tersebut akan “mendorong” elemen tersebut ke arah yang diinginkan</p>
<h3>Absolute</h3>
<p>Elemen akan dihilangkan / dicabut dari alur dokumen, dan elemen lainnya akan bekerja sebagaimana mestinya (tidak terganggu dengan elemen yang diberikan properti absolute)</p>
<p>Apabila element yang mempunyai properti absolute tersebut ada didalam sebuah element lagi (parent), maka element tersebut akan diabaikan, seakan-akan elemen tersebut tidak ada di dalam element parent tersebut</p>
<h3>Fixed</h3>
<p>Elemen akan dicabut / dilepas dari alur dokumen, hampir sama dengan elemen yang diberi properti absolute. Yang menjadi perbedaan, elemen yang diberikan posisi fixed akan selalu mengikuti (relative) dokumen, bukan element parent tertentu, atau dengan scroll halaman web.</p>
<p><img src=\"/intromagang/grav/user/pages/02.blog/04.perbedaan-css-position-static-relative-absolute-fixed-dan-inherit/css-position-696x392.jpg\" /></p>";
    }

    public function getTemplateName()
    {
        return "@Page:C:/xampp/htdocs/intromagang/grav/user/pages/02.blog/04.perbedaan-css-position-static-relative-absolute-fixed-dan-inherit";
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }
}
/* <p>Kali ini saya akan membahas apa sih perbedaan antara CSS Position Static, Relative, Absolute, Fixed dan Inherit.</p>*/
/* <p>Perlu diketahui sebelumnya, bahwa properti position memanipulasi lokasi elemen. Ada beberapa jenis position yang bisa kita tentukan, antara lain :</p>*/
/* <ul>*/
/* <li>Static</li>*/
/* <li>Relative</li>*/
/* <li>Absolute</li>*/
/* <li>Fixed</li>*/
/* <li>Inherit</li>*/
/* </ul>*/
/* <h3>Static</h3>*/
/* <p>Properti static merupakan properti bawaan semua elemen, dimana semua elemen akan memiliki properti ini. Dengan properti ini, elemen akan menempati posisi mengikuti alur halaman.</p>*/
/* <p>Jadi, apabila kita menambahkan value top, bottom, left, right, value tersebut tidak akan menghasilkan apapun.</p>*/
/* <h3>Relative</h3>*/
/* <p>Posisi elemen tetap mengikuti alur dokumen, hampir sama dengan value static. Yang menjadi perbedaan yaitu, value top, bottom, left, right akan berfungsi. Pengaturan posisi pada suatu elemen menggunakan value tersebut akan “mendorong” elemen tersebut ke arah yang diinginkan</p>*/
/* <h3>Absolute</h3>*/
/* <p>Elemen akan dihilangkan / dicabut dari alur dokumen, dan elemen lainnya akan bekerja sebagaimana mestinya (tidak terganggu dengan elemen yang diberikan properti absolute)</p>*/
/* <p>Apabila element yang mempunyai properti absolute tersebut ada didalam sebuah element lagi (parent), maka element tersebut akan diabaikan, seakan-akan elemen tersebut tidak ada di dalam element parent tersebut</p>*/
/* <h3>Fixed</h3>*/
/* <p>Elemen akan dicabut / dilepas dari alur dokumen, hampir sama dengan elemen yang diberi properti absolute. Yang menjadi perbedaan, elemen yang diberikan posisi fixed akan selalu mengikuti (relative) dokumen, bukan element parent tertentu, atau dengan scroll halaman web.</p>*/
/* <p><img src="/intromagang/grav/user/pages/02.blog/04.perbedaan-css-position-static-relative-absolute-fixed-dan-inherit/css-position-696x392.jpg" /></p>*/
