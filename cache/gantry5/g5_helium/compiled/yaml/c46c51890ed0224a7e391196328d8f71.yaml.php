<?php
return [
    '@class' => 'Gantry\\Component\\File\\CompiledYamlFile',
    'filename' => 'C:/xampp/htdocs/intromagang/grav/user/gantry5/themes/g5_helium/config/default/index.yaml',
    'modified' => 1470040370,
    'data' => [
        'name' => 'default',
        'timestamp' => 1470040370,
        'version' => 7,
        'preset' => [
            'image' => 'gantry-admin://images/layouts/default.png',
            'name' => 'default',
            'timestamp' => 1469626738
        ],
        'positions' => [
            'sidebar' => 'Sidebar'
        ],
        'sections' => [
            'navigation' => 'Navigation',
            'intro' => 'Intro',
            'features' => 'Features',
            'utility' => 'Utility',
            'above' => 'Above',
            'testimonials' => 'Testimonials',
            'expanded' => 'Expanded',
            'header' => 'Header',
            'mainbar' => 'Mainbar',
            'sidebar' => 'Sidebar',
            'footer' => 'Footer',
            'offcanvas' => 'Offcanvas'
        ],
        'particles' => [
            'logo' => [
                'logo-5263' => 'Logo / Image',
                'logo-9571' => 'Logo / Image'
            ],
            'menu' => [
                'menu-6409' => 'Menu'
            ],
            'social' => [
                'social-3171' => 'Social'
            ],
            'custom' => [
                'custom-4957' => 'Custom HTML'
            ],
            'content' => [
                'system-content-1587' => 'Page Content'
            ],
            'position' => [
                'position-position-3949' => 'Sidebar'
            ],
            'copyright' => [
                'copyright-1736' => 'Copyright'
            ],
            'totop' => [
                'totop-8670' => 'To Top'
            ],
            'mobile-menu' => [
                'mobile-menu-5697' => 'Mobile-menu'
            ]
        ],
        'inherit' => [
            
        ]
    ]
];
