<?php
return [
    '@class' => 'Gantry\\Component\\File\\CompiledYamlFile',
    'filename' => 'C:/xampp/htdocs/intromagang/grav/user/gantry5/themes/g5_helium/config/default/layout.yaml',
    'modified' => 1470040370,
    'data' => [
        'version' => 2,
        'preset' => [
            'image' => 'gantry-admin://images/layouts/default.png',
            'name' => 'default',
            'timestamp' => 1469626738
        ],
        'layout' => [
            '/navigation/' => [
                0 => [
                    0 => 'logo-5263 9',
                    1 => 'menu-6409 60',
                    2 => 'social-3171 17',
                    3 => 'custom-4957 14'
                ]
            ],
            '/header/' => [
                
            ],
            '/intro/' => [
                
            ],
            '/features/' => [
                
            ],
            '/utility/' => [
                
            ],
            '/above/' => [
                
            ],
            '/testimonials/' => [
                
            ],
            '/expanded/' => [
                
            ],
            '/container-main/' => [
                0 => [
                    0 => [
                        'mainbar 75' => [
                            0 => [
                                0 => 'system-content-1587'
                            ]
                        ]
                    ],
                    1 => [
                        'sidebar 25' => [
                            0 => [
                                0 => 'position-position-3949'
                            ]
                        ]
                    ]
                ]
            ],
            '/footer/' => [
                0 => [
                    0 => 'logo-9571 6',
                    1 => 'copyright-1736 78',
                    2 => 'totop-8670 16'
                ]
            ],
            '/offcanvas/' => [
                0 => [
                    0 => 'mobile-menu-5697'
                ]
            ]
        ],
        'structure' => [
            'navigation' => [
                'type' => 'section',
                'attributes' => [
                    'boxed' => ''
                ]
            ],
            'header' => [
                'attributes' => [
                    'boxed' => '',
                    'class' => ''
                ]
            ],
            'intro' => [
                'type' => 'section',
                'attributes' => [
                    'boxed' => ''
                ]
            ],
            'features' => [
                'type' => 'section',
                'attributes' => [
                    'boxed' => ''
                ]
            ],
            'utility' => [
                'type' => 'section',
                'attributes' => [
                    'boxed' => '',
                    'class' => ''
                ]
            ],
            'above' => [
                'type' => 'section',
                'attributes' => [
                    'boxed' => ''
                ]
            ],
            'testimonials' => [
                'type' => 'section',
                'attributes' => [
                    'boxed' => ''
                ]
            ],
            'expanded' => [
                'type' => 'section',
                'attributes' => [
                    'boxed' => ''
                ]
            ],
            'mainbar' => [
                'type' => 'section',
                'subtype' => 'main'
            ],
            'sidebar' => [
                'type' => 'section',
                'subtype' => 'aside'
            ],
            'container-main' => [
                'attributes' => [
                    'boxed' => ''
                ]
            ],
            'footer' => [
                'attributes' => [
                    'boxed' => ''
                ]
            ],
            'offcanvas' => [
                'attributes' => [
                    'boxed' => ''
                ]
            ]
        ],
        'content' => [
            'logo-5263' => [
                'title' => 'Logo / Image',
                'attributes' => [
                    'image' => 'gantry-media://5665654.jpg',
                    'svg' => ''
                ]
            ],
            'social-3171' => [
                'attributes' => [
                    'items' => [
                        0 => [
                            'icon' => 'fa fa-github fa-fw',
                            'text' => '',
                            'link' => 'http://www.github.com/akbaryu',
                            'name' => 'Twitter'
                        ],
                        1 => [
                            'icon' => 'fa fa-facebook fa-fw',
                            'text' => '',
                            'link' => 'http://www.facebook.com/akbaryu',
                            'name' => 'Facebook'
                        ],
                        2 => [
                            'icon' => 'fa fa-google-plus fa-fw',
                            'text' => '',
                            'link' => 'https://plus.google.com/+akbaryu',
                            'name' => 'Google+'
                        ]
                    ]
                ],
                'block' => [
                    'class' => 'g-social-header'
                ]
            ],
            'custom-4957' => [
                'title' => 'Custom HTML',
                'attributes' => [
                    'html' => '<a href="../personal">Back to PersonalWeb</a>'
                ]
            ],
            'position-position-3949' => [
                'title' => 'Sidebar',
                'attributes' => [
                    'key' => 'sidebar'
                ]
            ],
            'logo-9571' => [
                'title' => 'Logo / Image',
                'attributes' => [
                    'image' => 'gantry-media://5665654.jpg',
                    'svg' => ''
                ]
            ],
            'copyright-1736' => [
                'attributes' => [
                    'owner' => 'Akbaryu',
                    'additional' => [
                        'text' => 'Developed by Yudha exclusively<br />for FadilHore Team.'
                    ]
                ]
            ],
            'totop-8670' => [
                'title' => 'To Top'
            ]
        ]
    ]
];
