<?php
return [
    '@class' => 'Gantry\\Component\\Config\\CompiledBlueprints',
    'timestamp' => 1470033088,
    'checksum' => '7cacafeb48fe398118527bb1cfaeb65b',
    'files' => [
        'user/themes/g5_helium/particles' => [
            'particles/contentcubes' => [
                'file' => 'user/themes/g5_helium/particles/contentcubes.yaml',
                'modified' => 1469626738
            ],
            'particles/contenttabs' => [
                'file' => 'user/themes/g5_helium/particles/contenttabs.yaml',
                'modified' => 1469626738
            ],
            'particles/copyright' => [
                'file' => 'user/themes/g5_helium/particles/copyright.yaml',
                'modified' => 1469626738
            ],
            'particles/horizontalmenu' => [
                'file' => 'user/themes/g5_helium/particles/horizontalmenu.yaml',
                'modified' => 1469626738
            ],
            'particles/owlcarousel' => [
                'file' => 'user/themes/g5_helium/particles/owlcarousel.yaml',
                'modified' => 1469626738
            ]
        ],
        'user/plugins/gantry5/engines/nucleus/particles' => [
            'particles/analytics' => [
                'file' => 'user/plugins/gantry5/engines/nucleus/particles/analytics.yaml',
                'modified' => 1469626732
            ],
            'particles/assets' => [
                'file' => 'user/plugins/gantry5/engines/nucleus/particles/assets.yaml',
                'modified' => 1469626732
            ],
            'particles/branding' => [
                'file' => 'user/plugins/gantry5/engines/nucleus/particles/branding.yaml',
                'modified' => 1469626732
            ],
            'particles/content' => [
                'file' => 'user/plugins/gantry5/engines/nucleus/particles/content.yaml',
                'modified' => 1469626732
            ],
            'particles/copyright' => [
                'file' => 'user/plugins/gantry5/engines/nucleus/particles/copyright.yaml',
                'modified' => 1469626732
            ],
            'particles/custom' => [
                'file' => 'user/plugins/gantry5/engines/nucleus/particles/custom.yaml',
                'modified' => 1469626732
            ],
            'particles/date' => [
                'file' => 'user/plugins/gantry5/engines/nucleus/particles/date.yaml',
                'modified' => 1469626732
            ],
            'particles/logo' => [
                'file' => 'user/plugins/gantry5/engines/nucleus/particles/logo.yaml',
                'modified' => 1469626732
            ],
            'particles/menu' => [
                'file' => 'user/plugins/gantry5/engines/nucleus/particles/menu.yaml',
                'modified' => 1469626732
            ],
            'particles/messages' => [
                'file' => 'user/plugins/gantry5/engines/nucleus/particles/messages.yaml',
                'modified' => 1469626732
            ],
            'particles/mobile-menu' => [
                'file' => 'user/plugins/gantry5/engines/nucleus/particles/mobile-menu.yaml',
                'modified' => 1469626732
            ],
            'particles/position' => [
                'file' => 'user/plugins/gantry5/engines/nucleus/particles/position.yaml',
                'modified' => 1469626732
            ],
            'particles/social' => [
                'file' => 'user/plugins/gantry5/engines/nucleus/particles/social.yaml',
                'modified' => 1469626732
            ],
            'particles/spacer' => [
                'file' => 'user/plugins/gantry5/engines/nucleus/particles/spacer.yaml',
                'modified' => 1469626732
            ],
            'particles/totop' => [
                'file' => 'user/plugins/gantry5/engines/nucleus/particles/totop.yaml',
                'modified' => 1469626732
            ]
        ],
        'user/themes/g5_helium/blueprints' => [
            'blog_item' => [
                'file' => 'user/themes/g5_helium/blueprints/blog_item.yaml',
                'modified' => 1469626738
            ],
            'blog_list' => [
                'file' => 'user/themes/g5_helium/blueprints/blog_list.yaml',
                'modified' => 1469626738
            ],
            'default' => [
                'file' => 'user/themes/g5_helium/blueprints/default.yaml',
                'modified' => 1469626738
            ],
            'styles/above' => [
                'file' => 'user/themes/g5_helium/blueprints/styles/above.yaml',
                'modified' => 1469626738
            ],
            'styles/accent' => [
                'file' => 'user/themes/g5_helium/blueprints/styles/accent.yaml',
                'modified' => 1469626738
            ],
            'styles/base' => [
                'file' => 'user/themes/g5_helium/blueprints/styles/base.yaml',
                'modified' => 1469626738
            ],
            'styles/breakpoints' => [
                'file' => 'user/themes/g5_helium/blueprints/styles/breakpoints.yaml',
                'modified' => 1469626738
            ],
            'styles/expanded' => [
                'file' => 'user/themes/g5_helium/blueprints/styles/expanded.yaml',
                'modified' => 1469626738
            ],
            'styles/features' => [
                'file' => 'user/themes/g5_helium/blueprints/styles/features.yaml',
                'modified' => 1469626738
            ],
            'styles/font' => [
                'file' => 'user/themes/g5_helium/blueprints/styles/font.yaml',
                'modified' => 1469626738
            ],
            'styles/footer' => [
                'file' => 'user/themes/g5_helium/blueprints/styles/footer.yaml',
                'modified' => 1469626738
            ],
            'styles/header' => [
                'file' => 'user/themes/g5_helium/blueprints/styles/header.yaml',
                'modified' => 1469626738
            ],
            'styles/intro' => [
                'file' => 'user/themes/g5_helium/blueprints/styles/intro.yaml',
                'modified' => 1469626738
            ],
            'styles/menu' => [
                'file' => 'user/themes/g5_helium/blueprints/styles/menu.yaml',
                'modified' => 1469626738
            ],
            'styles/navigation' => [
                'file' => 'user/themes/g5_helium/blueprints/styles/navigation.yaml',
                'modified' => 1469626738
            ],
            'styles/offcanvas' => [
                'file' => 'user/themes/g5_helium/blueprints/styles/offcanvas.yaml',
                'modified' => 1469626738
            ],
            'styles/testimonials' => [
                'file' => 'user/themes/g5_helium/blueprints/styles/testimonials.yaml',
                'modified' => 1469626738
            ],
            'styles/utility' => [
                'file' => 'user/themes/g5_helium/blueprints/styles/utility.yaml',
                'modified' => 1469626738
            ]
        ],
        'user/plugins/gantry5/engines/nucleus/blueprints' => [
            'page/assets' => [
                'file' => 'user/plugins/gantry5/engines/nucleus/blueprints/page/assets.yaml',
                'modified' => 1469626732
            ],
            'page/body' => [
                'file' => 'user/plugins/gantry5/engines/nucleus/blueprints/page/body.yaml',
                'modified' => 1469626732
            ],
            'page/head' => [
                'file' => 'user/plugins/gantry5/engines/nucleus/blueprints/page/head.yaml',
                'modified' => 1469626732
            ],
            'pages/blog_item' => [
                'file' => 'user/plugins/gantry5/engines/nucleus/blueprints/pages/blog_item.yaml',
                'modified' => 1469626732
            ],
            'pages/blog_list' => [
                'file' => 'user/plugins/gantry5/engines/nucleus/blueprints/pages/blog_list.yaml',
                'modified' => 1469626732
            ],
            'pages/form' => [
                'file' => 'user/plugins/gantry5/engines/nucleus/blueprints/pages/form.yaml',
                'modified' => 1469626732
            ]
        ]
    ],
    'data' => [
        'items' => [
            'page.assets' => [
                'form' => [
                    
                ],
                'type' => '_root',
                'form_field' => false
            ],
            'page' => [
                'type' => '_parent',
                'name' => 'page',
                'form_field' => false
            ],
            'page.assets.favicon' => [
                'type' => 'input.imagepicker',
                'label' => 'Favicon',
                'filter' => '.(jpe?g|gif|png|svg|ico)$',
                'name' => 'page.assets.favicon'
            ],
            'page.assets.touchicon' => [
                'type' => 'input.imagepicker',
                'label' => 'Touch Icon',
                'description' => 'A PNG only image that will be used as icon for Touch Devices. Recommended 180x180 or 192x192.',
                'filter' => '.png$',
                'name' => 'page.assets.touchicon'
            ],
            'page.assets.css' => [
                'type' => 'collection.list',
                'array' => true,
                'label' => 'CSS',
                'description' => 'Add remove or modify custom CSS assets.',
                'value' => 'name',
                'ajax' => true,
                'name' => 'page.assets.css'
            ],
            'page.assets.css.*' => [
                'type' => '_parent',
                'name' => 'page.assets.css.*',
                'form_field' => false
            ],
            'page.assets.css.*.name' => [
                'type' => 'input.text',
                'label' => 'Name',
                'skip' => true,
                'name' => 'page.assets.css.*.name'
            ],
            'page.assets.css.*.location' => [
                'type' => 'input.filepicker',
                'label' => 'File Location',
                'icon' => 'fa-file-code-o',
                'placeholder' => 'http://cdn1.remote/file.css',
                'filter' => '\\.(css|less|scss|sass)$',
                'root' => 'gantry-assets://',
                'name' => 'page.assets.css.*.location'
            ],
            'page.assets.css.*.inline' => [
                'type' => 'textarea.textarea',
                'label' => 'Inline CSS',
                'description' => 'Adds inline CSS for quick snippets.',
                'name' => 'page.assets.css.*.inline'
            ],
            'page.assets._info' => [
                'type' => 'separator.note',
                'class' => 'alert alert-info',
                'content' => 'Only add your inline JavaScript code, the &lt;script&gt;&lt;/script&gt; tags will get automatically added for you.',
                'name' => 'page.assets._info'
            ],
            'page.assets.extra' => [
                'type' => 'collection.keyvalue',
                'label' => 'Tag attributes',
                'key_placeholder' => 'Key (data-*, style, ...)',
                'value_placeholder' => 'Value',
                'exclude' => [
                    0 => 'rel',
                    1 => 'href',
                    2 => 'type'
                ],
                'name' => 'page.assets.extra'
            ],
            'page.assets.priority' => [
                'type' => 'input.number',
                'label' => 'Load Priority',
                'description' => 'Sets the load priority of the asset in the page. Value can be between 10 (first) and -10 (last). Default value is 0.',
                'default' => 0,
                'min' => -10,
                'max' => 10,
                'name' => 'page.assets.priority'
            ],
            'page.assets.javascript' => [
                'type' => 'collection.list',
                'array' => true,
                'label' => 'Javascript',
                'description' => 'Add remove or modify custom Javascript assets.',
                'value' => 'name',
                'ajax' => true,
                'name' => 'page.assets.javascript'
            ],
            'page.assets.javascript.*' => [
                'type' => '_parent',
                'name' => 'page.assets.javascript.*',
                'form_field' => false
            ],
            'page.assets.javascript.*.name' => [
                'type' => 'input.text',
                'label' => 'Name',
                'skip' => true,
                'name' => 'page.assets.javascript.*.name'
            ],
            'page.assets.javascript.*.location' => [
                'type' => 'input.filepicker',
                'label' => 'File Location',
                'icon' => 'fa-file-code-o',
                'placeholder' => 'http://cdn1.remote/file.js',
                'filter' => '\\.(jsx?|coffee)$',
                'root' => 'gantry-assets://',
                'name' => 'page.assets.javascript.*.location'
            ],
            'page.assets.javascript.*.inline' => [
                'type' => 'textarea.textarea',
                'label' => 'Inline JavaScript',
                'description' => 'Adds inline JavaScript for quick snippets.',
                'name' => 'page.assets.javascript.*.inline'
            ],
            'page.assets.javascript.*.in_footer' => [
                'type' => 'input.checkbox',
                'label' => 'Before </body>',
                'description' => 'Whether you want the script to load at the end of the body tag or inside head',
                'default' => false,
                'name' => 'page.assets.javascript.*.in_footer'
            ],
            'page.assets.javascript.*.extra' => [
                'type' => 'collection.keyvalue',
                'label' => 'Tag attributes',
                'exclude' => [
                    0 => 'src',
                    1 => 'type'
                ],
                'name' => 'page.assets.javascript.*.extra'
            ],
            'page.assets.javascript.*.priority' => [
                'type' => 'input.number',
                'label' => 'Load Priority',
                'description' => 'Sets the load priority of the asset in the page. Value can be between 10 (first) and -10 (last). Default value is 0.',
                'default' => 0,
                'min' => -10,
                'max' => 10,
                'name' => 'page.assets.javascript.*.priority'
            ],
            'page.body' => [
                'form' => [
                    
                ],
                'type' => '_root',
                'form_field' => false
            ],
            'page.body.doctype' => [
                'type' => 'input.hidden',
                'label' => 'Doctype',
                'default' => 'html',
                'name' => 'page.body.doctype'
            ],
            'page.body.attribs' => [
                'type' => '_parent',
                'name' => 'page.body.attribs',
                'form_field' => false
            ],
            'page.body.attribs.id' => [
                'type' => 'input.text',
                'label' => 'Body Id',
                'default' => NULL,
                'name' => 'page.body.attribs.id'
            ],
            'page.body.attribs.class' => [
                'type' => 'input.selectize',
                'label' => 'Body Classes',
                'default' => 'gantry',
                'name' => 'page.body.attribs.class'
            ],
            'page.body.attribs.extra' => [
                'type' => 'collection.keyvalue',
                'label' => 'Tag Attributes',
                'description' => 'Extra Tag attributes.',
                'key_placeholder' => 'Key (data-*, style, ...)',
                'value_placeholder' => 'Value',
                'exclude' => [
                    0 => 'id',
                    1 => 'class'
                ],
                'name' => 'page.body.attribs.extra'
            ],
            'page.body.layout' => [
                'type' => '_parent',
                'name' => 'page.body.layout',
                'form_field' => false
            ],
            'page.body.layout.sections' => [
                'type' => 'select.selectize',
                'label' => 'Sections Layout',
                'description' => 'Default layout container behavior for Sections',
                'default' => 0,
                'options' => [
                    0 => 'Fullwidth (Boxed Content)',
                    2 => 'Fullwidth (Flushed Content)',
                    1 => 'Boxed',
                    3 => 'Remove Container'
                ],
                'name' => 'page.body.layout.sections'
            ],
            'page.body.body_top' => [
                'type' => 'textarea.textarea',
                'label' => 'After <body>',
                'description' => 'Anything in this field will be appended right after the opening body tag',
                'name' => 'page.body.body_top'
            ],
            'page.body.body_bottom' => [
                'type' => 'textarea.textarea',
                'label' => 'Before </body>',
                'description' => 'Anything in this field will be appended right before the closing body tag',
                'name' => 'page.body.body_bottom'
            ],
            'page.head' => [
                'form' => [
                    
                ],
                'type' => '_root',
                'form_field' => false
            ],
            'page.head.meta' => [
                'type' => 'collection.keyvalue',
                'label' => 'Meta Tags',
                'description' => 'Meta Tags for extras such as Facebook and Twitter.',
                'key_placeholder' => 'og:title, og:site_name, twitter:site',
                'value_placeholder' => 'Value',
                'default' => NULL,
                'name' => 'page.head.meta'
            ],
            'page.head.head_bottom' => [
                'type' => 'textarea.textarea',
                'label' => 'Custom Content',
                'description' => 'Anything in this field will be appended to the head tag',
                'name' => 'page.head.head_bottom'
            ],
            'page.head.atoms' => [
                'type' => 'input.hidden',
                'override_target' => '#atoms .atoms-list + input[type="checkbox"]',
                'array' => true,
                'name' => 'page.head.atoms'
            ],
            'pages.blog_item' => [
                'form' => [
                    
                ],
                'type' => '_root',
                'form_field' => false
            ],
            'pages' => [
                'type' => '_parent',
                'name' => 'pages',
                'form_field' => false
            ],
            'pages.blog_item.header_image' => [
                'type' => 'section',
                'title' => 'Header Image',
                'underline' => true,
                'name' => 'pages.blog_item.header_image'
            ],
            'pages.blog_item.header' => [
                'type' => '_parent',
                'name' => 'pages.blog_item.header',
                'form_field' => false
            ],
            'pages.blog_item.header.header_image' => [
                'type' => 'toggle',
                'toggleable' => true,
                'label' => 'Display Header Image',
                'help' => 'Enabled displaying of a header image',
                'highlight' => 1,
                'options' => [
                    1 => 'PLUGIN_ADMIN.ENABLED',
                    0 => 'PLUGIN_ADMIN.DISABLED'
                ],
                'name' => 'pages.blog_item.header.header_image'
            ],
            'pages.blog_item.header.header_image_file' => [
                'type' => 'text',
                'toggleable' => true,
                'label' => 'Image File',
                'help' => 'image filename that exists in the page folder. If not provided, will use the first image found.',
                'placeholder' => 'For example: myimage.jpg',
                'name' => 'pages.blog_item.header.header_image_file'
            ],
            'pages.blog_item.header.header_image_width' => [
                'type' => 'text',
                'toggleable' => true,
                'label' => 'Image Width',
                'size' => 'small',
                'help' => 'Header width in px',
                'placeholder' => 'Default is 900',
                'validate' => [
                    'type' => 'int',
                    'min' => 0,
                    'max' => 5000
                ],
                'name' => 'pages.blog_item.header.header_image_width'
            ],
            'pages.blog_item.header.header_image_height' => [
                'type' => 'text',
                'toggleable' => true,
                'label' => 'Image Height',
                'size' => 'small',
                'help' => 'Header height in px',
                'placeholder' => 'Default is 300',
                'validate' => [
                    'type' => 'int',
                    'min' => 0,
                    'max' => 5000
                ],
                'name' => 'pages.blog_item.header.header_image_height'
            ],
            'pages.blog_item.summary' => [
                'type' => 'section',
                'title' => 'Summary',
                'underline' => true,
                'name' => 'pages.blog_item.summary'
            ],
            'pages.blog_item.header.summary' => [
                'type' => '_parent',
                'name' => 'pages.blog_item.header.summary',
                'form_field' => false
            ],
            'pages.blog_item.header.summary.enabled' => [
                'type' => 'toggle',
                'toggleable' => true,
                'label' => 'Summary',
                'highlight' => 1,
                'options' => [
                    1 => 'PLUGIN_ADMIN.ENABLED',
                    0 => 'PLUGIN_ADMIN.DISABLED'
                ],
                'name' => 'pages.blog_item.header.summary.enabled'
            ],
            'pages.blog_item.header.summary.format' => [
                'type' => 'select',
                'toggleable' => true,
                'label' => 'Format',
                'classes' => 'fancy',
                'options' => [
                    'short' => 'Use the first occurence of delimter or size',
                    'long' => 'Summary delimiter will be ignored'
                ],
                'name' => 'pages.blog_item.header.summary.format'
            ],
            'pages.blog_item.header.summary.size' => [
                'type' => 'text',
                'toggleable' => true,
                'label' => 'Size',
                'classes' => 'large',
                'placeholder' => 300,
                'validate' => [
                    'type' => 'int',
                    'min' => 1
                ],
                'name' => 'pages.blog_item.header.summary.size'
            ],
            'pages.blog_item.header.summary.delimiter' => [
                'type' => 'text',
                'toggleable' => true,
                'label' => 'Summary delimiter',
                'classes' => 'large',
                'placeholder' => '===',
                'name' => 'pages.blog_item.header.summary.delimiter'
            ],
            'pages.blog_item.blog' => [
                'type' => 'tab',
                'title' => 'Blog Item',
                'name' => 'pages.blog_item.blog'
            ],
            'pages.blog_item.tabs' => [
                'name' => 'pages.blog_item.tabs'
            ],
            'pages.blog_list' => [
                'form' => [
                    
                ],
                'type' => '_root',
                'form_field' => false
            ],
            'pages.blog_list.header' => [
                'type' => '_parent',
                'name' => 'pages.blog_list.header',
                'form_field' => false
            ],
            'pages.blog_list.header.child_type' => [
                'default' => 'blog_item',
                'name' => 'pages.blog_list.header.child_type'
            ],
            'pages.blog_list.overrides' => [
                'name' => 'pages.blog_list.overrides'
            ],
            'pages.blog_list.advanced' => [
                'name' => 'pages.blog_list.advanced'
            ],
            'pages.blog_list.header.content' => [
                'type' => '_parent',
                'name' => 'pages.blog_list.header.content',
                'form_field' => false
            ],
            'pages.blog_list.header.content.items' => [
                'type' => 'textarea',
                'yaml' => true,
                'label' => 'Items',
                'default' => '@self.children',
                'name' => 'pages.blog_list.header.content.items'
            ],
            'pages.blog_list.header.content.limit' => [
                'type' => 'text',
                'label' => 'Max Item Count',
                'default' => 5,
                'validate' => [
                    'required' => true,
                    'type' => 'int',
                    'min' => 1
                ],
                'name' => 'pages.blog_list.header.content.limit'
            ],
            'pages.blog_list.header.content.order' => [
                'type' => '_parent',
                'name' => 'pages.blog_list.header.content.order',
                'form_field' => false
            ],
            'pages.blog_list.header.content.order.by' => [
                'type' => 'select',
                'label' => 'Order By',
                'default' => 'date',
                'options' => [
                    'folder' => 'Folder',
                    'title' => 'Title',
                    'date' => 'Date',
                    'default' => 'Default'
                ],
                'name' => 'pages.blog_list.header.content.order.by'
            ],
            'pages.blog_list.header.content.order.dir' => [
                'type' => 'select',
                'label' => 'Order',
                'default' => 'desc',
                'options' => [
                    'asc' => 'Ascending',
                    'desc' => 'Descending'
                ],
                'name' => 'pages.blog_list.header.content.order.dir'
            ],
            'pages.blog_list.header.content.pagination' => [
                'type' => 'toggle',
                'label' => 'Pagination',
                'highlight' => 1,
                'default' => 1,
                'options' => [
                    1 => 'PLUGIN_ADMIN.ENABLED',
                    0 => 'PLUGIN_ADMIN.DISABLED'
                ],
                'validate' => [
                    'type' => 'bool'
                ],
                'name' => 'pages.blog_list.header.content.pagination'
            ],
            'pages.blog_list.header.content.url_taxonomy_filters' => [
                'type' => 'toggle',
                'label' => 'URL Taxonomy Filters',
                'highlight' => 1,
                'default' => 1,
                'options' => [
                    1 => 'PLUGIN_ADMIN.ENABLED',
                    0 => 'PLUGIN_ADMIN.DISABLED'
                ],
                'validate' => [
                    'type' => 'bool'
                ],
                'name' => 'pages.blog_list.header.content.url_taxonomy_filters'
            ],
            'pages.blog_list.blog' => [
                'type' => 'tab',
                'title' => 'Blog List',
                'name' => 'pages.blog_list.blog'
            ],
            'pages.blog_list.tabs' => [
                'type' => 'tabs',
                'active' => 1,
                'name' => 'pages.blog_list.tabs'
            ],
            'pages.form' => [
                'form' => [
                    
                ],
                'type' => '_root',
                'form_field' => false
            ],
            'blog_item' => [
                'form' => [
                    
                ],
                'type' => '_root',
                'form_field' => false
            ],
            'blog_item.header_image' => [
                'type' => 'section',
                'title' => 'Header Image',
                'underline' => true,
                'name' => 'blog_item.header_image'
            ],
            'blog_item.header' => [
                'type' => '_parent',
                'name' => 'blog_item.header',
                'form_field' => false
            ],
            'blog_item.header.header_image' => [
                'replace@' => true,
                'type' => 'toggle',
                'toggleable' => true,
                'label' => 'Display Header Image',
                'help' => 'Enabled displaying of a header image',
                'highlight' => 1,
                'options' => [
                    1 => 'PLUGIN_ADMIN.ENABLED',
                    0 => 'PLUGIN_ADMIN.DISABLED'
                ],
                'name' => 'blog_item.header.header_image'
            ],
            'blog_item.header.header_image_file' => [
                'type' => 'text',
                'toggleable' => true,
                'label' => 'Image File',
                'help' => 'image filename that exists in the page folder. If not provided, will use the first image found.',
                'placeholder' => 'For example: myimage.jpg',
                'name' => 'blog_item.header.header_image_file'
            ],
            'blog_item.header.header_image_width' => [
                'type' => 'text',
                'toggleable' => true,
                'label' => 'Image Width',
                'size' => 'small',
                'help' => 'Header width in px',
                'placeholder' => 'Default is 900',
                'validate' => [
                    'type' => 'int',
                    'min' => 0,
                    'max' => 5000
                ],
                'name' => 'blog_item.header.header_image_width'
            ],
            'blog_item.header.header_image_height' => [
                'type' => 'text',
                'toggleable' => true,
                'label' => 'Image Height',
                'size' => 'small',
                'help' => 'Header height in px',
                'placeholder' => 'Default is 300',
                'validate' => [
                    'type' => 'int',
                    'min' => 0,
                    'max' => 5000
                ],
                'name' => 'blog_item.header.header_image_height'
            ],
            'blog_item.summary' => [
                'type' => 'section',
                'title' => 'Summary',
                'underline' => true,
                'name' => 'blog_item.summary'
            ],
            'blog_item.header.summary' => [
                'type' => '_parent',
                'name' => 'blog_item.header.summary',
                'form_field' => false
            ],
            'blog_item.header.summary.enabled' => [
                'replace@' => true,
                'type' => 'toggle',
                'toggleable' => true,
                'label' => 'Summary',
                'highlight' => 1,
                'options' => [
                    1 => 'PLUGIN_ADMIN.ENABLED',
                    0 => 'PLUGIN_ADMIN.DISABLED'
                ],
                'name' => 'blog_item.header.summary.enabled'
            ],
            'blog_item.header.summary.format' => [
                'type' => 'select',
                'toggleable' => true,
                'label' => 'Format',
                'classes' => 'fancy',
                'options' => [
                    'short' => 'Use the first occurence of delimter or size',
                    'long' => 'Summary delimiter will be ignored'
                ],
                'name' => 'blog_item.header.summary.format'
            ],
            'blog_item.header.summary.size' => [
                'type' => 'text',
                'toggleable' => true,
                'label' => 'Size',
                'classes' => 'large',
                'placeholder' => 300,
                'validate' => [
                    'type' => 'int',
                    'min' => 1
                ],
                'name' => 'blog_item.header.summary.size'
            ],
            'blog_item.header.summary.delimiter' => [
                'type' => 'text',
                'toggleable' => true,
                'label' => 'Summary delimiter',
                'classes' => 'large',
                'placeholder' => '===',
                'name' => 'blog_item.header.summary.delimiter'
            ],
            'blog_item.blog' => [
                'type' => 'tab',
                'title' => 'Blog Item',
                'name' => 'blog_item.blog'
            ],
            'blog_item.tabs' => [
                'name' => 'blog_item.tabs'
            ],
            'blog_list' => [
                'form' => [
                    
                ],
                'type' => '_root',
                'form_field' => false
            ],
            'blog_list.header' => [
                'type' => '_parent',
                'name' => 'blog_list.header',
                'form_field' => false
            ],
            'blog_list.header.child_type' => [
                'default' => 'blog_item',
                'name' => 'blog_list.header.child_type'
            ],
            'blog_list.overrides' => [
                'name' => 'blog_list.overrides'
            ],
            'blog_list.advanced' => [
                'name' => 'blog_list.advanced'
            ],
            'blog_list.header.content' => [
                'type' => '_parent',
                'name' => 'blog_list.header.content',
                'form_field' => false
            ],
            'blog_list.header.content.items' => [
                'type' => 'textarea',
                'yaml' => true,
                'label' => 'Items',
                'default' => '@self.children',
                'name' => 'blog_list.header.content.items'
            ],
            'blog_list.header.content.limit' => [
                'type' => 'text',
                'label' => 'Max Item Count',
                'default' => 5,
                'validate' => [
                    'required' => true,
                    'type' => 'int',
                    'min' => 1
                ],
                'name' => 'blog_list.header.content.limit'
            ],
            'blog_list.header.content.order' => [
                'type' => '_parent',
                'name' => 'blog_list.header.content.order',
                'form_field' => false
            ],
            'blog_list.header.content.order.by' => [
                'type' => 'select',
                'label' => 'Order By',
                'default' => 'date',
                'options' => [
                    'folder' => 'Folder',
                    'title' => 'Title',
                    'date' => 'Date',
                    'default' => 'Default'
                ],
                'name' => 'blog_list.header.content.order.by'
            ],
            'blog_list.header.content.order.dir' => [
                'type' => 'select',
                'label' => 'Order',
                'default' => 'desc',
                'options' => [
                    'asc' => 'Ascending',
                    'desc' => 'Descending'
                ],
                'name' => 'blog_list.header.content.order.dir'
            ],
            'blog_list.header.content.pagination' => [
                'type' => 'toggle',
                'label' => 'Pagination',
                'highlight' => 1,
                'default' => 1,
                'options' => [
                    1 => 'PLUGIN_ADMIN.ENABLED',
                    0 => 'PLUGIN_ADMIN.DISABLED'
                ],
                'validate' => [
                    'type' => 'bool'
                ],
                'name' => 'blog_list.header.content.pagination'
            ],
            'blog_list.header.content.url_taxonomy_filters' => [
                'type' => 'toggle',
                'label' => 'URL Taxonomy Filters',
                'highlight' => 1,
                'default' => 1,
                'options' => [
                    1 => 'PLUGIN_ADMIN.ENABLED',
                    0 => 'PLUGIN_ADMIN.DISABLED'
                ],
                'validate' => [
                    'type' => 'bool'
                ],
                'name' => 'blog_list.header.content.url_taxonomy_filters'
            ],
            'blog_list.blog' => [
                'type' => 'tab',
                'title' => 'Blog List',
                'name' => 'blog_list.blog'
            ],
            'blog_list.tabs' => [
                'type' => 'tabs',
                'active' => 1,
                'name' => 'blog_list.tabs'
            ],
            'default' => [
                'form' => [
                    
                ],
                'type' => '_root',
                'form_field' => false
            ],
            'default.header' => [
                'type' => '_parent',
                'name' => 'default.header',
                'form_field' => false
            ],
            'default.header.child_type' => [
                'default' => 'blog_item',
                'name' => 'default.header.child_type'
            ],
            'default.overrides' => [
                'name' => 'default.overrides'
            ],
            'default.advanced' => [
                'name' => 'default.advanced'
            ],
            'default.header.content' => [
                'type' => '_parent',
                'name' => 'default.header.content',
                'form_field' => false
            ],
            'default.header.content.items' => [
                'type' => 'textarea',
                'yaml' => true,
                'label' => 'Items',
                'default' => '@self.children',
                'name' => 'default.header.content.items'
            ],
            'default.header.content.limit' => [
                'type' => 'text',
                'label' => 'Max Item Count',
                'default' => 5,
                'validate' => [
                    'required' => true,
                    'type' => 'int',
                    'min' => 1
                ],
                'name' => 'default.header.content.limit'
            ],
            'default.header.content.order' => [
                'type' => '_parent',
                'name' => 'default.header.content.order',
                'form_field' => false
            ],
            'default.header.content.order.by' => [
                'type' => 'select',
                'label' => 'Order By',
                'default' => 'date',
                'options' => [
                    'folder' => 'Folder',
                    'title' => 'Title',
                    'date' => 'Date',
                    'default' => 'Default'
                ],
                'name' => 'default.header.content.order.by'
            ],
            'default.header.content.order.dir' => [
                'type' => 'select',
                'label' => 'Order',
                'default' => 'desc',
                'options' => [
                    'asc' => 'Ascending',
                    'desc' => 'Descending'
                ],
                'name' => 'default.header.content.order.dir'
            ],
            'default.header.content.pagination' => [
                'type' => 'toggle',
                'label' => 'Pagination',
                'highlight' => 1,
                'default' => 1,
                'options' => [
                    1 => 'PLUGIN_ADMIN.ENABLED',
                    0 => 'PLUGIN_ADMIN.DISABLED'
                ],
                'validate' => [
                    'type' => 'bool'
                ],
                'name' => 'default.header.content.pagination'
            ],
            'default.header.content.url_taxonomy_filters' => [
                'type' => 'toggle',
                'label' => 'URL Taxonomy Filters',
                'highlight' => 1,
                'default' => 1,
                'options' => [
                    1 => 'PLUGIN_ADMIN.ENABLED',
                    0 => 'PLUGIN_ADMIN.DISABLED'
                ],
                'validate' => [
                    'type' => 'bool'
                ],
                'name' => 'default.header.content.url_taxonomy_filters'
            ],
            'default.blog' => [
                'type' => 'tab',
                'title' => 'Blog List',
                'name' => 'default.blog'
            ],
            'default.tabs' => [
                'type' => 'tabs',
                'active' => 1,
                'name' => 'default.tabs'
            ],
            'styles.above' => [
                'form' => [
                    
                ],
                'type' => '_root',
                'form_field' => false
            ],
            'styles' => [
                'type' => '_parent',
                'name' => 'styles',
                'form_field' => false
            ],
            'styles.above.background' => [
                'type' => 'input.colorpicker',
                'label' => 'Background',
                'default' => '#f4f5f7',
                'name' => 'styles.above.background'
            ],
            'styles.above.text-color' => [
                'type' => 'input.colorpicker',
                'label' => 'Text',
                'default' => '#424753',
                'name' => 'styles.above.text-color'
            ],
            'styles.accent' => [
                'form' => [
                    
                ],
                'type' => '_root',
                'form_field' => false
            ],
            'styles.accent.color-1' => [
                'type' => 'input.colorpicker',
                'label' => 'Accent Color 1',
                'default' => '#4db2b3',
                'name' => 'styles.accent.color-1'
            ],
            'styles.accent.color-2' => [
                'type' => 'input.colorpicker',
                'label' => 'Accent Color 2',
                'default' => '#8f4dae',
                'name' => 'styles.accent.color-2'
            ],
            'styles.base' => [
                'form' => [
                    
                ],
                'type' => '_root',
                'form_field' => false
            ],
            'styles.base.background' => [
                'type' => 'input.colorpicker',
                'label' => 'Base Background',
                'default' => '#ffffff',
                'name' => 'styles.base.background'
            ],
            'styles.base.text-color' => [
                'type' => 'input.colorpicker',
                'label' => 'Base Text Color',
                'default' => '#424753',
                'name' => 'styles.base.text-color'
            ],
            'styles.base.favicon' => [
                'type' => 'input.imagepicker',
                'label' => 'Favicon',
                'filter' => '.(jpe?g|gif|png|svg|ico)$',
                'name' => 'styles.base.favicon'
            ],
            'styles.breakpoints' => [
                'form' => [
                    
                ],
                'type' => '_root',
                'form_field' => false
            ],
            'styles.breakpoints.large-desktop-container' => [
                'type' => 'input.text',
                'label' => 'Large Desktop',
                'description' => 'Set breakpoint size in rem, em, or px unit values',
                'default' => '75rem',
                'pattern' => '\\d+(\\.\\d+){0,1}(rem|em|ex|ch|vw|vh|vmin|vmax|%|px|cm|mm|in|pt|pc)',
                'name' => 'styles.breakpoints.large-desktop-container'
            ],
            'styles.breakpoints.desktop-container' => [
                'type' => 'input.text',
                'label' => 'Desktop',
                'description' => 'Set breakpoint size in rem, em, or px unit values',
                'default' => '60rem',
                'pattern' => '\\d+(\\.\\d+){0,1}(rem|em|ex|ch|vw|vh|vmin|vmax|%|px|cm|mm|in|pt|pc)',
                'name' => 'styles.breakpoints.desktop-container'
            ],
            'styles.breakpoints.tablet-container' => [
                'type' => 'input.text',
                'label' => 'Tablet',
                'description' => 'Set breakpoint size in rem, em, or px unit values',
                'default' => '48rem',
                'pattern' => '\\d+(\\.\\d+){0,1}(rem|em|ex|ch|vw|vh|vmin|vmax|%|px|cm|mm|in|pt|pc)',
                'name' => 'styles.breakpoints.tablet-container'
            ],
            'styles.breakpoints.large-mobile-container' => [
                'type' => 'input.text',
                'label' => 'Mobile',
                'description' => 'Set breakpoint size in rem, em, or px unit values',
                'default' => '30rem',
                'pattern' => '\\d+(\\.\\d+){0,1}(rem|em|ex|ch|vw|vh|vmin|vmax|%|px|cm|mm|in|pt|pc)',
                'name' => 'styles.breakpoints.large-mobile-container'
            ],
            'styles.breakpoints.mobile-menu-breakpoint' => [
                'type' => 'input.text',
                'label' => 'Mobile Menu',
                'description' => 'Set breakpoint size in rem, em, or px unit values',
                'default' => '48rem',
                'pattern' => '\\d+(\\.\\d+){0,1}(rem|em|ex|ch|vw|vh|vmin|vmax|%|px|cm|mm|in|pt|pc)',
                'name' => 'styles.breakpoints.mobile-menu-breakpoint'
            ],
            'styles.expanded' => [
                'form' => [
                    
                ],
                'type' => '_root',
                'form_field' => false
            ],
            'styles.expanded.background' => [
                'type' => 'input.colorpicker',
                'label' => 'Background',
                'default' => '#ffffff',
                'name' => 'styles.expanded.background'
            ],
            'styles.expanded.text-color' => [
                'type' => 'input.colorpicker',
                'label' => 'Text',
                'default' => '#424753',
                'name' => 'styles.expanded.text-color'
            ],
            'styles.features' => [
                'form' => [
                    
                ],
                'type' => '_root',
                'form_field' => false
            ],
            'styles.features.background' => [
                'type' => 'input.colorpicker',
                'label' => 'Background',
                'default' => '#ffffff',
                'name' => 'styles.features.background'
            ],
            'styles.features.text-color' => [
                'type' => 'input.colorpicker',
                'label' => 'Text',
                'default' => '#424753',
                'name' => 'styles.features.text-color'
            ],
            'styles.font' => [
                'form' => [
                    
                ],
                'type' => '_root',
                'form_field' => false
            ],
            'styles.font.family-default' => [
                'type' => 'input.fonts',
                'label' => 'Body Font',
                'default' => 'Raleway, Helvetica, Tahoma, Geneva, Arial, sans-serif',
                'name' => 'styles.font.family-default'
            ],
            'styles.font.family-title' => [
                'type' => 'input.fonts',
                'label' => 'Title Font',
                'default' => 'Lato, Helvetica, Tahoma, Geneva, Arial, sans-serif',
                'name' => 'styles.font.family-title'
            ],
            'styles.footer' => [
                'form' => [
                    
                ],
                'type' => '_root',
                'form_field' => false
            ],
            'styles.footer.background' => [
                'type' => 'input.colorpicker',
                'label' => 'Background',
                'default' => '#312f38',
                'name' => 'styles.footer.background'
            ],
            'styles.footer.text-color' => [
                'type' => 'input.colorpicker',
                'label' => 'Text',
                'default' => '#ffffff',
                'name' => 'styles.footer.text-color'
            ],
            'styles.header' => [
                'form' => [
                    
                ],
                'type' => '_root',
                'form_field' => false
            ],
            'styles.header.background' => [
                'type' => 'input.colorpicker',
                'label' => 'Background',
                'default' => '#312f38',
                'name' => 'styles.header.background'
            ],
            'styles.header.background-image' => [
                'type' => 'input.imagepicker',
                'label' => 'Background Image',
                'default' => 'gantry-media://header/img01.jpg',
                'name' => 'styles.header.background-image'
            ],
            'styles.header.background-overlay' => [
                'type' => 'select.select',
                'label' => 'Background Overlay',
                'description' => 'Enables the linear gradient overlay made of accent colors.',
                'placeholder' => 'Select...',
                'default' => 'enabled',
                'options' => [
                    'enabled' => 'Enabled',
                    'disabled' => 'Disabled'
                ],
                'name' => 'styles.header.background-overlay'
            ],
            'styles.header.text-color' => [
                'type' => 'input.colorpicker',
                'label' => 'Text',
                'default' => '#eceeef',
                'name' => 'styles.header.text-color'
            ],
            'styles.intro' => [
                'form' => [
                    
                ],
                'type' => '_root',
                'form_field' => false
            ],
            'styles.intro.background' => [
                'type' => 'input.colorpicker',
                'label' => 'Background',
                'default' => '#f4f5f7',
                'name' => 'styles.intro.background'
            ],
            'styles.intro.text-color' => [
                'type' => 'input.colorpicker',
                'label' => 'Text',
                'default' => '#424753',
                'name' => 'styles.intro.text-color'
            ],
            'styles.menu' => [
                'form' => [
                    
                ],
                'type' => '_root',
                'form_field' => false
            ],
            'styles.menu.col-width' => [
                'type' => 'input.text',
                'label' => 'Simple Dropdown Width',
                'description' => 'Specify the default width of menu dropdowns for simple mode in rem, em or px units. This width can be overridden on each individual menu item from the menu editor.',
                'default' => '180px',
                'pattern' => '\\d+(\\.\\d+){0,1}(rem|em|px)',
                'name' => 'styles.menu.col-width'
            ],
            'styles.menu.animation' => [
                'type' => 'select.selectize',
                'label' => 'Dropdown Animation',
                'description' => 'Select the dropdown animation.',
                'default' => 'g-fade',
                'options' => [
                    'g-no-animation' => 'No Animation',
                    'g-fade' => 'Fade',
                    'g-zoom' => 'Zoom',
                    'g-fade-in-up' => 'Fade In Up',
                    'g-dropdown-bounce-in-down' => 'Bounce In Down',
                    'g-dropdown-bounce-in-left' => 'Bounce In Left',
                    'g-dropdown-bounce-in-right' => 'Bounce In Right'
                ],
                'name' => 'styles.menu.animation'
            ],
            'styles.navigation' => [
                'form' => [
                    
                ],
                'type' => '_root',
                'form_field' => false
            ],
            'styles.navigation.background' => [
                'type' => 'input.colorpicker',
                'label' => 'Background',
                'default' => '#312f38',
                'name' => 'styles.navigation.background'
            ],
            'styles.navigation.text-color' => [
                'type' => 'input.colorpicker',
                'label' => 'Text',
                'default' => '#ffffff',
                'name' => 'styles.navigation.text-color'
            ],
            'styles.offcanvas' => [
                'form' => [
                    
                ],
                'type' => '_root',
                'form_field' => false
            ],
            'styles.offcanvas.background' => [
                'type' => 'input.colorpicker',
                'label' => 'Background',
                'default' => '#4db2b3',
                'name' => 'styles.offcanvas.background'
            ],
            'styles.offcanvas.text-color' => [
                'type' => 'input.colorpicker',
                'label' => 'Text',
                'default' => '#eceeef',
                'name' => 'styles.offcanvas.text-color'
            ],
            'styles.offcanvas.toggle-color' => [
                'type' => 'input.colorpicker',
                'label' => 'Toggle Color',
                'default' => '#4db2b3',
                'name' => 'styles.offcanvas.toggle-color'
            ],
            'styles.offcanvas.toggle-visibility' => [
                'type' => 'select.selectize',
                'label' => 'Toggle Visibility',
                'description' => 'Choose the OffCanvas Toggle Visibility.',
                'default' => 1,
                'options' => [
                    1 => 'Mobile Menu',
                    2 => 'Always'
                ],
                'name' => 'styles.offcanvas.toggle-visibility'
            ],
            'styles.offcanvas.width' => [
                'type' => 'input.text',
                'label' => 'Panel Width',
                'description' => 'Set offcanvas size in rem, em, px, or percentage unit values',
                'default' => '12rem',
                'pattern' => '\\d+(\\.\\d+){0,1}(rem|em|ex|ch|vw|vh|vmin|vmax|%|px|cm|mm|in|pt|pc)',
                'name' => 'styles.offcanvas.width'
            ],
            'styles.testimonials' => [
                'form' => [
                    
                ],
                'type' => '_root',
                'form_field' => false
            ],
            'styles.testimonials.background' => [
                'type' => 'input.colorpicker',
                'label' => 'Background',
                'default' => '#8f4dae',
                'name' => 'styles.testimonials.background'
            ],
            'styles.testimonials.background-image' => [
                'type' => 'input.imagepicker',
                'label' => 'Background Image',
                'default' => 'gantry-media://testimonials/img01.jpg',
                'name' => 'styles.testimonials.background-image'
            ],
            'styles.testimonials.background-overlay' => [
                'type' => 'select.select',
                'label' => 'Background Overlay',
                'description' => 'Enables the linear gradient overlay made of accent colors.',
                'placeholder' => 'Select...',
                'default' => 'enabled',
                'options' => [
                    'enabled' => 'Enabled',
                    'disabled' => 'Disabled'
                ],
                'name' => 'styles.testimonials.background-overlay'
            ],
            'styles.testimonials.text-color' => [
                'type' => 'input.colorpicker',
                'label' => 'Text',
                'default' => '#eceeef',
                'name' => 'styles.testimonials.text-color'
            ],
            'styles.utility' => [
                'form' => [
                    
                ],
                'type' => '_root',
                'form_field' => false
            ],
            'styles.utility.background' => [
                'type' => 'input.colorpicker',
                'label' => 'Background',
                'default' => '#424753',
                'name' => 'styles.utility.background'
            ],
            'styles.utility.text-color' => [
                'type' => 'input.colorpicker',
                'label' => 'Text',
                'default' => '#ffffff',
                'name' => 'styles.utility.text-color'
            ],
            'particles.analytics' => [
                'form' => [
                    
                ],
                'type' => '_root',
                'form_field' => false
            ],
            'particles' => [
                'type' => '_parent',
                'name' => 'particles',
                'form_field' => false
            ],
            'particles.analytics.enabled' => [
                'type' => 'input.checkbox',
                'label' => 'Enabled',
                'description' => 'Globally enable analytic particles.',
                'default' => true,
                'name' => 'particles.analytics.enabled'
            ],
            'particles.analytics.ua' => [
                'type' => '_parent',
                'name' => 'particles.analytics.ua',
                'form_field' => false
            ],
            'particles.analytics.ua.code' => [
                'type' => 'input.text',
                'description' => 'Enter the Google UA tracking code for analytics (UA-XXXXXXXX-X)',
                'label' => 'UA Code',
                'placeholder' => 'UA-XXXXXXXX-X',
                'name' => 'particles.analytics.ua.code'
            ],
            'particles.analytics.ua.anonym' => [
                'type' => 'input.checkbox',
                'description' => 'Send only Anonymous IP Addresses (mandatory in Europe)',
                'label' => 'Anonym Statistics',
                'default' => false,
                'name' => 'particles.analytics.ua.anonym'
            ],
            'particles.analytics.ua.ssl' => [
                'type' => 'input.checkbox',
                'description' => 'Send all data using SSL, even from insecure (HTTP) pages.',
                'label' => 'Force SSL use',
                'default' => false,
                'name' => 'particles.analytics.ua.ssl'
            ],
            'particles.analytics.ua.debug' => [
                'type' => 'input.checkbox',
                'description' => 'Enable the debug version of analytics.js - DON\'T USE ON PRODUCTION!',
                'label' => 'Use Debug Mode',
                'default' => false,
                'name' => 'particles.analytics.ua.debug'
            ],
            'particles.assets' => [
                'form' => [
                    
                ],
                'type' => '_root',
                'form_field' => false
            ],
            'particles.assets.enabled' => [
                'type' => 'input.checkbox',
                'label' => 'Enabled',
                'description' => 'Globally enable CSS/JS particles.',
                'default' => true,
                'name' => 'particles.assets.enabled'
            ],
            'particles.assets.css' => [
                'type' => 'collection.list',
                'array' => true,
                'label' => 'CSS',
                'description' => 'Add remove or modify custom CSS assets.',
                'value' => 'name',
                'ajax' => true,
                'name' => 'particles.assets.css'
            ],
            'particles.assets.css.*' => [
                'type' => '_parent',
                'name' => 'particles.assets.css.*',
                'form_field' => false
            ],
            'particles.assets.css.*.name' => [
                'type' => 'input.text',
                'label' => 'Name',
                'skip' => true,
                'name' => 'particles.assets.css.*.name'
            ],
            'particles.assets.css.*.location' => [
                'type' => 'input.filepicker',
                'label' => 'File Location',
                'icon' => 'fa-file-code-o',
                'placeholder' => 'http://cdn1.remote/file.css',
                'filter' => '\\.(css|less|scss|sass)$',
                'root' => 'gantry-assets://',
                'name' => 'particles.assets.css.*.location'
            ],
            'particles.assets.css.*.inline' => [
                'type' => 'textarea.textarea',
                'label' => 'Inline CSS',
                'description' => 'Adds inline CSS for quick snippets.',
                'name' => 'particles.assets.css.*.inline'
            ],
            'particles.assets._info' => [
                'type' => 'separator.note',
                'class' => 'alert alert-info',
                'content' => 'Only add your inline JavaScript code, the &lt;script&gt;&lt;/script&gt; tags will get automatically added for you.',
                'name' => 'particles.assets._info'
            ],
            'particles.assets.extra' => [
                'type' => 'collection.keyvalue',
                'label' => 'Tag attributes',
                'key_placeholder' => 'Key (data-*, style, ...)',
                'value_placeholder' => 'Value',
                'exclude' => [
                    0 => 'rel',
                    1 => 'href',
                    2 => 'type'
                ],
                'name' => 'particles.assets.extra'
            ],
            'particles.assets.priority' => [
                'type' => 'input.number',
                'label' => 'Load Priority',
                'description' => 'Sets the load priority of the asset in the page. Value can be between 10 (first) and -10 (last). Default value is 0.',
                'default' => 0,
                'min' => -10,
                'max' => 10,
                'name' => 'particles.assets.priority'
            ],
            'particles.assets.javascript' => [
                'type' => 'collection.list',
                'array' => true,
                'label' => 'Javascript',
                'description' => 'Add remove or modify custom Javascript assets.',
                'value' => 'name',
                'ajax' => true,
                'name' => 'particles.assets.javascript'
            ],
            'particles.assets.javascript.*' => [
                'type' => '_parent',
                'name' => 'particles.assets.javascript.*',
                'form_field' => false
            ],
            'particles.assets.javascript.*.name' => [
                'type' => 'input.text',
                'label' => 'Name',
                'skip' => true,
                'name' => 'particles.assets.javascript.*.name'
            ],
            'particles.assets.javascript.*.location' => [
                'type' => 'input.filepicker',
                'label' => 'File Location',
                'icon' => 'fa-file-code-o',
                'placeholder' => 'http://cdn1.remote/file.js',
                'filter' => '\\.(jsx?|coffee)$',
                'root' => 'gantry-assets://',
                'name' => 'particles.assets.javascript.*.location'
            ],
            'particles.assets.javascript.*.inline' => [
                'type' => 'textarea.textarea',
                'label' => 'Inline JavaScript',
                'description' => 'Adds inline JavaScript for quick snippets.',
                'name' => 'particles.assets.javascript.*.inline'
            ],
            'particles.assets.javascript.*.in_footer' => [
                'type' => 'input.checkbox',
                'label' => 'Before </body>',
                'description' => 'Whether you want the script to load at the end of the body tag or inside head',
                'default' => false,
                'name' => 'particles.assets.javascript.*.in_footer'
            ],
            'particles.assets.javascript.*.extra' => [
                'type' => 'collection.keyvalue',
                'label' => 'Tag attributes',
                'exclude' => [
                    0 => 'src',
                    1 => 'type'
                ],
                'name' => 'particles.assets.javascript.*.extra'
            ],
            'particles.assets.javascript.*.priority' => [
                'type' => 'input.number',
                'label' => 'Load Priority',
                'description' => 'Sets the load priority of the asset in the page. Value can be between 10 (first) and -10 (last). Default value is 0.',
                'default' => 0,
                'min' => -10,
                'max' => 10,
                'name' => 'particles.assets.javascript.*.priority'
            ],
            'particles.branding' => [
                'form' => [
                    
                ],
                'type' => '_root',
                'form_field' => false
            ],
            'particles.branding.enabled' => [
                'type' => 'input.checkbox',
                'label' => 'Enabled',
                'description' => 'Globally enable to the particles.',
                'default' => true,
                'name' => 'particles.branding.enabled'
            ],
            'particles.branding.content' => [
                'type' => 'textarea.textarea',
                'label' => 'Content',
                'description' => 'Create or modify custom branding content.',
                'default' => 'Powered by <a href="http://www.gantry.org/" title="Gantry Framework" class="g-powered-by">Gantry Framework</a>',
                'name' => 'particles.branding.content'
            ],
            'particles.branding.css' => [
                'type' => '_parent',
                'name' => 'particles.branding.css',
                'form_field' => false
            ],
            'particles.branding.css.class' => [
                'type' => 'input.selectize',
                'label' => 'CSS Classes',
                'description' => 'CSS class name for the particle.',
                'default' => 'branding',
                'name' => 'particles.branding.css.class'
            ],
            'particles.content' => [
                'form' => [
                    
                ],
                'type' => '_root',
                'form_field' => false
            ],
            'particles.content.enabled' => [
                'type' => 'input.checkbox',
                'label' => 'Enabled',
                'description' => 'Globally enable page content.',
                'default' => true,
                'name' => 'particles.content.enabled'
            ],
            'particles.content._info' => [
                'type' => 'separator.note',
                'class' => 'alert alert-info',
                'content' => 'Displays the main page content in your layout.',
                'name' => 'particles.content._info'
            ],
            'particles.content._alert' => [
                'type' => 'separator.note',
                'class' => 'alert alert-warning',
                'content' => 'This particle is needed to display the main page content, though it may be disabled from a few selected pages like your front page.',
                'name' => 'particles.content._alert'
            ],
            'particles.copyright' => [
                'form' => [
                    
                ],
                'type' => '_root',
                'form_field' => false
            ],
            'particles.copyright.enabled' => [
                'type' => 'input.checkbox',
                'label' => 'Enabled',
                'description' => 'Globally enable the particle.',
                'default' => true,
                'name' => 'particles.copyright.enabled'
            ],
            'particles.copyright.date' => [
                'type' => '_parent',
                'name' => 'particles.copyright.date',
                'form_field' => false
            ],
            'particles.copyright.date.start' => [
                'type' => 'input.text',
                'label' => 'Start Year',
                'description' => 'Select the copyright start year.',
                'default' => 'now',
                'name' => 'particles.copyright.date.start'
            ],
            'particles.copyright.date.end' => [
                'type' => 'input.text',
                'label' => 'End Year',
                'description' => 'Select the copyright end year.',
                'default' => 'now',
                'name' => 'particles.copyright.date.end'
            ],
            'particles.copyright.owner' => [
                'type' => 'input.text',
                'label' => 'Copyright owner',
                'description' => 'Add copyright owner name.',
                'name' => 'particles.copyright.owner'
            ],
            'particles.custom' => [
                'form' => [
                    
                ],
                'type' => '_root',
                'form_field' => false
            ],
            'particles.custom.enabled' => [
                'type' => 'input.checkbox',
                'label' => 'Enabled',
                'description' => 'Globally enable the particle.',
                'default' => true,
                'name' => 'particles.custom.enabled'
            ],
            'particles.custom.html' => [
                'type' => 'textarea.textarea',
                'label' => 'Custom HTML',
                'description' => 'Enter custom HTML into here.',
                'overridable' => false,
                'name' => 'particles.custom.html'
            ],
            'particles.custom.twig' => [
                'type' => 'input.checkbox',
                'label' => 'Process Twig',
                'description' => 'Enable Twig template processing in the content. Twig will be processed before shortcodes.',
                'name' => 'particles.custom.twig'
            ],
            'particles.custom.filter' => [
                'type' => 'input.checkbox',
                'label' => 'Process shortcodes',
                'description' => 'Enable shortcode processing / filtering in the content.',
                'name' => 'particles.custom.filter'
            ],
            'particles.date' => [
                'form' => [
                    
                ],
                'type' => '_root',
                'form_field' => false
            ],
            'particles.date.enabled' => [
                'type' => 'input.checkbox',
                'label' => 'Enabled',
                'description' => 'Globally enable date particles.',
                'default' => true,
                'name' => 'particles.date.enabled'
            ],
            'particles.date.css' => [
                'type' => '_parent',
                'name' => 'particles.date.css',
                'form_field' => false
            ],
            'particles.date.css.class' => [
                'type' => 'input.selectize',
                'label' => 'CSS Classes',
                'description' => 'CSS class name for the particle.',
                'default' => 'date',
                'name' => 'particles.date.css.class'
            ],
            'particles.date.date' => [
                'type' => '_parent',
                'name' => 'particles.date.date',
                'form_field' => false
            ],
            'particles.date.date.formats' => [
                'type' => 'select.date',
                'label' => 'Format',
                'description' => 'Select preferred date format.',
                'default' => 'l, F d, Y',
                'placeholder' => 'Select...',
                'selectize' => [
                    'allowEmptyOption' => true
                ],
                'options' => [
                    'l, F d, Y' => 'Date1',
                    'l, d F' => 'Date2',
                    'D, d F' => 'Date3',
                    'F d' => 'Date4',
                    'd F' => 'Date5',
                    'd M' => 'Date6',
                    'D, M d, Y' => 'Date7',
                    'D, M d, y' => 'Date8',
                    'l' => 'Date9',
                    'l j F Y' => 'Date10',
                    'j F Y' => 'Date11'
                ],
                'name' => 'particles.date.date.formats'
            ],
            'particles.logo' => [
                'form' => [
                    
                ],
                'type' => '_root',
                'form_field' => false
            ],
            'particles.logo.enabled' => [
                'type' => 'input.checkbox',
                'label' => 'Enabled',
                'description' => 'Globally enable logo particles.',
                'default' => true,
                'name' => 'particles.logo.enabled'
            ],
            'particles.logo.url' => [
                'type' => 'input.text',
                'label' => 'Url',
                'description' => 'Url for the image. Leave empty to go to home page.',
                'name' => 'particles.logo.url'
            ],
            'particles.logo.image' => [
                'type' => 'input.imagepicker',
                'label' => 'Image',
                'description' => 'Select desired logo image.',
                'name' => 'particles.logo.image'
            ],
            'particles.logo.svg' => [
                'type' => 'textarea.textarea',
                'label' => 'SVG Code',
                'description' => 'Your SVG code that will be added inline to the site.',
                'placeholder' => 'Place your <svg> code here.',
                'name' => 'particles.logo.svg'
            ],
            'particles.logo.text' => [
                'type' => 'input.text',
                'label' => 'Text',
                'description' => 'Input logo description text.',
                'name' => 'particles.logo.text'
            ],
            'particles.logo.class' => [
                'type' => 'input.selectize',
                'label' => 'CSS Classes',
                'description' => 'Set a specific CSS class for custom styling.',
                'name' => 'particles.logo.class'
            ],
            'particles.menu' => [
                'form' => [
                    
                ],
                'type' => '_root',
                'form_field' => false
            ],
            'particles.menu.enabled' => [
                'type' => 'input.checkbox',
                'label' => 'Enabled',
                'description' => 'Globally enable the menu particle.',
                'default' => true,
                'name' => 'particles.menu.enabled'
            ],
            'particles.menu._info' => [
                'type' => 'separator.note',
                'class' => 'alert alert-info',
                'content' => 'GANTRY5_PARTICLE_MENU_INFO',
                'name' => 'particles.menu._info'
            ],
            'particles.menu.menu' => [
                'type' => 'menu.list',
                'label' => 'Menu',
                'description' => 'Select menu to be used with the particle.',
                'default' => '',
                'selectize' => [
                    'allowEmptyOption' => true
                ],
                'options' => [
                    '' => 'Use Default Menu',
                    '-active-' => 'Use Active Menu'
                ],
                'name' => 'particles.menu.menu'
            ],
            'particles.menu.base' => [
                'type' => 'menu.item',
                'label' => 'Base Item',
                'description' => 'Select a menu item to always be used as the base for the menu display.',
                'default' => '/',
                'options' => [
                    '/' => 'Active'
                ],
                'name' => 'particles.menu.base'
            ],
            'particles.menu.startLevel' => [
                'type' => 'input.text',
                'label' => 'Start Level',
                'description' => 'Set the start level of the menu.',
                'default' => 1,
                'name' => 'particles.menu.startLevel'
            ],
            'particles.menu.maxLevels' => [
                'type' => 'input.text',
                'label' => 'Max Levels',
                'description' => 'Set the maximum number of menu levels to display.',
                'default' => 0,
                'name' => 'particles.menu.maxLevels'
            ],
            'particles.menu.renderTitles' => [
                'type' => 'input.checkbox',
                'label' => 'Render Titles',
                'description' => 'Renders the titles/tooltips of the Menu Items for accessibility.',
                'default' => 0,
                'name' => 'particles.menu.renderTitles'
            ],
            'particles.menu.hoverExpand' => [
                'type' => 'input.checkbox',
                'label' => 'Expand on Hover',
                'description' => 'Allows to enable / disable the ability to expand menu items by hover or click only',
                'default' => 1,
                'name' => 'particles.menu.hoverExpand'
            ],
            'particles.menu.mobileTarget' => [
                'type' => 'input.checkbox',
                'label' => 'Mobile Target',
                'description' => 'Check this field if you want this menu to become the target for Mobile Menu and to appear in Offcanvas',
                'default' => 0,
                'name' => 'particles.menu.mobileTarget'
            ],
            'particles.messages' => [
                'form' => [
                    
                ],
                'type' => '_root',
                'form_field' => false
            ],
            'particles.messages.enabled' => [
                'type' => 'input.checkbox',
                'label' => 'Enabled',
                'description' => 'Globally enable system messages.',
                'default' => true,
                'name' => 'particles.messages.enabled'
            ],
            'particles.messages._info' => [
                'type' => 'separator.note',
                'class' => 'alert alert-info',
                'content' => 'Displays system messages in your layout.',
                'name' => 'particles.messages._info'
            ],
            'particles.messages._alert' => [
                'type' => 'separator.note',
                'class' => 'alert alert-warning',
                'content' => 'Always include this particle to all of your layouts. Otherwise users will not see important system messages like login failures.',
                'name' => 'particles.messages._alert'
            ],
            'particles.mobile-menu' => [
                'form' => [
                    
                ],
                'type' => '_root',
                'form_field' => false
            ],
            'particles.mobile-menu.enabled' => [
                'type' => 'input.checkbox',
                'label' => 'Enabled',
                'description' => 'Globally enable spacer.',
                'default' => true,
                'name' => 'particles.mobile-menu.enabled'
            ],
            'particles.mobile-menu._note' => [
                'type' => 'separator.note',
                'class' => 'alert alert-info',
                'content' => '<p>This Particle is the container target where, on Mobile, the Menu will be injected.</p><p>Please note that this Particle <strong>must</strong> be unique in the Layout and positioned in the Offcanvas section. You will also need a Menu present in the Layout.</p>',
                'name' => 'particles.mobile-menu._note'
            ],
            'particles.position' => [
                'form' => [
                    
                ],
                'type' => '_root',
                'form_field' => false
            ],
            'particles.position.enabled' => [
                'type' => 'input.checkbox',
                'label' => 'Enabled',
                'description' => 'Globally enable module positions.',
                'default' => true,
                'name' => 'particles.position.enabled'
            ],
            'particles.position.key' => [
                'type' => 'input.text',
                'label' => 'Key',
                'description' => 'Position name.',
                'pattern' => '[A-Za-z0-9-]+',
                'overridable' => false,
                'name' => 'particles.position.key'
            ],
            'particles.position.chrome' => [
                'type' => 'input.text',
                'label' => 'Chrome',
                'description' => 'Module chrome in this position.',
                'placeholder' => 'gantry',
                'name' => 'particles.position.chrome'
            ],
            'particles.social' => [
                'form' => [
                    
                ],
                'type' => '_root',
                'form_field' => false
            ],
            'particles.social.enabled' => [
                'type' => 'input.checkbox',
                'label' => 'Enabled',
                'description' => 'Globally enable social particles.',
                'default' => true,
                'name' => 'particles.social.enabled'
            ],
            'particles.social.css' => [
                'type' => '_parent',
                'name' => 'particles.social.css',
                'form_field' => false
            ],
            'particles.social.css.class' => [
                'type' => 'input.selectize',
                'label' => 'CSS Classes',
                'description' => 'CSS class name for the particle.',
                'default' => 'social',
                'name' => 'particles.social.css.class'
            ],
            'particles.social.title' => [
                'type' => 'input.text',
                'label' => 'Title',
                'description' => 'Customize the title text.',
                'placeholder' => 'Enter title',
                'name' => 'particles.social.title'
            ],
            'particles.social.target' => [
                'type' => 'select.selectize',
                'label' => 'Target',
                'description' => 'Target browser window when item is clicked.',
                'placeholder' => 'Select...',
                'default' => '_blank',
                'options' => [
                    '_parent' => 'Self',
                    '_blank' => 'New Window'
                ],
                'name' => 'particles.social.target'
            ],
            'particles.social.items' => [
                'type' => 'collection.list',
                'array' => true,
                'label' => 'Social Items',
                'description' => 'Create each social item to display.',
                'value' => 'name',
                'ajax' => true,
                'name' => 'particles.social.items'
            ],
            'particles.social.items.*' => [
                'type' => '_parent',
                'name' => 'particles.social.items.*',
                'form_field' => false
            ],
            'particles.social.items.*.name' => [
                'type' => 'input.text',
                'label' => 'Name',
                'skip' => true,
                'name' => 'particles.social.items.*.name'
            ],
            'particles.social.items.*.icon' => [
                'type' => 'input.icon',
                'label' => 'Icon',
                'name' => 'particles.social.items.*.icon'
            ],
            'particles.social.items.*.text' => [
                'type' => 'input.text',
                'label' => 'Text',
                'name' => 'particles.social.items.*.text'
            ],
            'particles.social.items.*.link' => [
                'type' => 'input.text',
                'label' => 'Link',
                'name' => 'particles.social.items.*.link'
            ],
            'particles.spacer' => [
                'form' => [
                    
                ],
                'type' => '_root',
                'form_field' => false
            ],
            'particles.spacer.enabled' => [
                'type' => 'input.checkbox',
                'label' => 'Enabled',
                'description' => 'Globally enable spacer.',
                'default' => true,
                'name' => 'particles.spacer.enabled'
            ],
            'particles.spacer._info' => [
                'type' => 'separator.note',
                'class' => 'alert alert-info',
                'content' => 'Add empty spacer column to the row. Shows up as an empty space in your layout.',
                'name' => 'particles.spacer._info'
            ],
            'particles.totop' => [
                'form' => [
                    
                ],
                'type' => '_root',
                'form_field' => false
            ],
            'particles.totop.enabled' => [
                'type' => 'checkbox',
                'label' => 'Enabled',
                'description' => 'Globally enable to top particles.',
                'default' => true,
                'name' => 'particles.totop.enabled'
            ],
            'particles.totop.css' => [
                'type' => '_parent',
                'name' => 'particles.totop.css',
                'form_field' => false
            ],
            'particles.totop.css.class' => [
                'type' => 'input.selectize',
                'label' => 'CSS Classes',
                'description' => 'CSS class name for the particle.',
                'default' => 'totop',
                'name' => 'particles.totop.css.class'
            ],
            'particles.totop.icon' => [
                'type' => 'input.icon',
                'label' => 'Icon',
                'description' => 'A Font Awesome icon to be displayed for the link.',
                'name' => 'particles.totop.icon'
            ],
            'particles.totop.content' => [
                'type' => 'input.text',
                'label' => 'Text',
                'description' => 'The text to be displayed for the link. HTML is allowed.',
                'placeholder' => 'To Top',
                'name' => 'particles.totop.content'
            ],
            'particles.contentcubes' => [
                'form' => [
                    
                ],
                'type' => '_root',
                'form_field' => false
            ],
            'particles.contentcubes.enabled' => [
                'type' => 'input.checkbox',
                'label' => 'Enabled',
                'description' => 'Globally enable the particle.',
                'default' => true,
                'name' => 'particles.contentcubes.enabled'
            ],
            'particles.contentcubes.css' => [
                'type' => '_parent',
                'name' => 'particles.contentcubes.css',
                'form_field' => false
            ],
            'particles.contentcubes.css.class' => [
                'type' => 'input.text',
                'label' => 'CSS Classes',
                'description' => 'CSS class name for the particle.',
                'name' => 'particles.contentcubes.css.class'
            ],
            'particles.contentcubes.title' => [
                'type' => 'input.text',
                'label' => 'Title',
                'description' => 'Customize the title text.',
                'placeholder' => 'Enter title',
                'name' => 'particles.contentcubes.title'
            ],
            'particles.contentcubes.items' => [
                'type' => 'collection.list',
                'array' => true,
                'label' => 'Content Items',
                'description' => 'Create each item to display.',
                'value' => 'name',
                'ajax' => true,
                'name' => 'particles.contentcubes.items'
            ],
            'particles.contentcubes.items.*' => [
                'type' => '_parent',
                'name' => 'particles.contentcubes.items.*',
                'form_field' => false
            ],
            'particles.contentcubes.items.*.image' => [
                'type' => 'input.imagepicker',
                'label' => 'Image',
                'description' => 'Select desired image.',
                'name' => 'particles.contentcubes.items.*.image'
            ],
            'particles.contentcubes.items.*.imageposition' => [
                'type' => 'select.select',
                'label' => 'Image Position',
                'description' => 'Should the image appear on the left or right of the content.',
                'default' => 'left',
                'options' => [
                    'left' => 'Left',
                    'right' => 'Right'
                ],
                'name' => 'particles.contentcubes.items.*.imageposition'
            ],
            'particles.contentcubes.items.*.label' => [
                'type' => 'input.text',
                'label' => 'Label',
                'description' => 'Enter the label that appears on top of the title',
                'name' => 'particles.contentcubes.items.*.label'
            ],
            'particles.contentcubes.items.*.title' => [
                'type' => 'input.text',
                'label' => 'Title',
                'description' => 'Enter the title',
                'name' => 'particles.contentcubes.items.*.title'
            ],
            'particles.contentcubes.items.*.tags' => [
                'type' => 'collection.list',
                'array' => true,
                'label' => 'Tags',
                'description' => 'Create each tag to display.',
                'value' => 'text',
                'ajax' => true,
                'name' => 'particles.contentcubes.items.*.tags'
            ],
            'particles.contentcubes.items.*.tags.*' => [
                'type' => '_parent',
                'name' => 'particles.contentcubes.items.*.tags.*',
                'form_field' => false
            ],
            'particles.contentcubes.items.*.tags.*.text' => [
                'type' => 'input.text',
                'label' => 'Tag Text',
                'description' => 'Input the text to be used as tag.',
                'name' => 'particles.contentcubes.items.*.tags.*.text'
            ],
            'particles.contentcubes.items.*.tags.*.icon' => [
                'type' => 'input.icon',
                'label' => 'Tag Icon',
                'description' => 'Input the icon.',
                'default' => 'fa fa-tag',
                'name' => 'particles.contentcubes.items.*.tags.*.icon'
            ],
            'particles.contentcubes.items.*.tags.*.link' => [
                'type' => 'input.text',
                'label' => 'Tag Link',
                'description' => 'Specify the tag link.',
                'name' => 'particles.contentcubes.items.*.tags.*.link'
            ],
            'particles.contentcubes.items.*.tags.*.target' => [
                'type' => 'select.selectize',
                'label' => 'Target',
                'description' => 'Target browser window when item is clicked.',
                'placeholder' => 'Select...',
                'default' => '_self',
                'options' => [
                    '_self' => 'Self',
                    '_blank' => 'New Window'
                ],
                'name' => 'particles.contentcubes.items.*.tags.*.target'
            ],
            'particles.contentcubes.items.*.link' => [
                'type' => 'input.text',
                'label' => 'Link',
                'description' => 'Input the item link.',
                'name' => 'particles.contentcubes.items.*.link'
            ],
            'particles.contentcubes.items.*.linktext' => [
                'type' => 'input.text',
                'label' => 'Link Text',
                'description' => 'Input the text for the item link.',
                'default' => '&rarr;',
                'name' => 'particles.contentcubes.items.*.linktext'
            ],
            'particles.contentcubes.items.*.buttonclass' => [
                'type' => 'input.text',
                'label' => 'Button Class',
                'description' => 'Input the button class.',
                'default' => '',
                'name' => 'particles.contentcubes.items.*.buttonclass'
            ],
            'particles.contentcubes.items.*.buttontarget' => [
                'type' => 'select.selectize',
                'label' => 'Button Target',
                'description' => 'Target browser window when item is clicked.',
                'placeholder' => 'Select...',
                'default' => '_self',
                'options' => [
                    '_self' => 'Self',
                    '_blank' => 'New Window'
                ],
                'name' => 'particles.contentcubes.items.*.buttontarget'
            ],
            'particles.contenttabs' => [
                'form' => [
                    
                ],
                'type' => '_root',
                'form_field' => false
            ],
            'particles.contenttabs.enabled' => [
                'type' => 'input.checkbox',
                'label' => 'Enabled',
                'description' => 'Globally enable Content Tabs particle.',
                'default' => true,
                'name' => 'particles.contenttabs.enabled'
            ],
            'particles.contenttabs.class' => [
                'type' => 'input.selectize',
                'label' => 'CSS Classes',
                'description' => 'CSS class name for the particle.',
                'name' => 'particles.contenttabs.class'
            ],
            'particles.contenttabs.title' => [
                'type' => 'input.text',
                'label' => 'Title',
                'description' => 'Customize the particle title text.',
                'placeholder' => 'Enter title',
                'name' => 'particles.contenttabs.title'
            ],
            'particles.contenttabs.animation' => [
                'type' => 'select.select',
                'label' => 'Animation Type',
                'description' => 'Set the animation type.',
                'default' => 'slide',
                'options' => [
                    'left' => 'Slide Left',
                    'right' => 'Slide Right',
                    'up' => 'Slide Up',
                    'down' => 'Slide Down',
                    'fade' => 'Fade',
                    'toggle' => 'Toggle'
                ],
                'name' => 'particles.contenttabs.animation'
            ],
            'particles.contenttabs.items' => [
                'type' => 'collection.list',
                'array' => true,
                'label' => 'Content Tabs Items',
                'description' => 'Content Tabs item to display.',
                'value' => 'title',
                'ajax' => true,
                'name' => 'particles.contenttabs.items'
            ],
            'particles.contenttabs.items.*' => [
                'type' => '_parent',
                'name' => 'particles.contenttabs.items.*',
                'form_field' => false
            ],
            'particles.contenttabs.items.*.title' => [
                'type' => 'input.text',
                'label' => 'Title',
                'description' => 'Enter the title',
                'name' => 'particles.contenttabs.items.*.title'
            ],
            'particles.contenttabs.items.*.content' => [
                'type' => 'textarea.textarea',
                'label' => 'Tab Content',
                'description' => 'Customize the tab content.',
                'placeholder' => 'Enter your custom content here.',
                'name' => 'particles.contenttabs.items.*.content'
            ],
            'particles.copyright.link' => [
                'type' => 'input.text',
                'label' => 'Owner Link',
                'description' => 'Add link for owner.',
                'name' => 'particles.copyright.link'
            ],
            'particles.copyright.target' => [
                'type' => 'select.select',
                'label' => 'Owner Link Target',
                'description' => 'Target browser window when owner link is clicked.',
                'placeholder' => 'Select...',
                'default' => '_blank',
                'options' => [
                    '_parent' => 'Self',
                    '_blank' => 'New Window'
                ],
                'name' => 'particles.copyright.target'
            ],
            'particles.copyright.additional' => [
                'type' => '_parent',
                'name' => 'particles.copyright.additional',
                'form_field' => false
            ],
            'particles.copyright.additional.text' => [
                'type' => 'input.text',
                'label' => 'Additional Text',
                'description' => 'Additional text that you\'d like to add below the copyright.',
                'name' => 'particles.copyright.additional.text'
            ],
            'particles.copyright.css' => [
                'type' => '_parent',
                'name' => 'particles.copyright.css',
                'form_field' => false
            ],
            'particles.copyright.css.class' => [
                'type' => 'input.text',
                'label' => 'CSS Classes',
                'description' => 'CSS class name for the particle.',
                'name' => 'particles.copyright.css.class'
            ],
            'particles.horizontalmenu' => [
                'form' => [
                    
                ],
                'type' => '_root',
                'form_field' => false
            ],
            'particles.horizontalmenu.enabled' => [
                'type' => 'input.checkbox',
                'label' => 'Enabled',
                'description' => 'Globally enable icon menu particles.',
                'default' => true,
                'name' => 'particles.horizontalmenu.enabled'
            ],
            'particles.horizontalmenu.class' => [
                'type' => 'input.selectize',
                'label' => 'CSS Classes',
                'description' => 'CSS class name for the particle.',
                'name' => 'particles.horizontalmenu.class'
            ],
            'particles.horizontalmenu.target' => [
                'type' => 'select.select',
                'label' => 'Target',
                'description' => 'Target browser window when item is clicked.',
                'placeholder' => 'Select...',
                'default' => '_blank',
                'options' => [
                    '_parent' => 'Self',
                    '_blank' => 'New Window'
                ],
                'name' => 'particles.horizontalmenu.target'
            ],
            'particles.horizontalmenu.items' => [
                'type' => 'collection.list',
                'array' => true,
                'label' => 'Menu Items',
                'description' => 'Create each menu item to display.',
                'value' => 'name',
                'ajax' => true,
                'name' => 'particles.horizontalmenu.items'
            ],
            'particles.horizontalmenu.items.*' => [
                'type' => '_parent',
                'name' => 'particles.horizontalmenu.items.*',
                'form_field' => false
            ],
            'particles.horizontalmenu.items.*.name' => [
                'type' => 'input.text',
                'label' => 'Name',
                'skip' => true,
                'name' => 'particles.horizontalmenu.items.*.name'
            ],
            'particles.horizontalmenu.items.*.text' => [
                'type' => 'input.text',
                'label' => 'Text',
                'name' => 'particles.horizontalmenu.items.*.text'
            ],
            'particles.horizontalmenu.items.*.link' => [
                'type' => 'input.text',
                'label' => 'Link',
                'name' => 'particles.horizontalmenu.items.*.link'
            ],
            'particles.owlcarousel' => [
                'form' => [
                    
                ],
                'type' => '_root',
                'form_field' => false
            ],
            'particles.owlcarousel.enabled' => [
                'type' => 'input.checkbox',
                'label' => 'Enabled',
                'description' => 'Globally enable icon menu particles.',
                'default' => true,
                'name' => 'particles.owlcarousel.enabled'
            ],
            'particles.owlcarousel.class' => [
                'type' => 'input.selectize',
                'label' => 'CSS Classes',
                'description' => 'CSS class name for the particle.',
                'name' => 'particles.owlcarousel.class'
            ],
            'particles.owlcarousel.title' => [
                'type' => 'input.text',
                'label' => 'Title',
                'description' => 'Customize the title text.',
                'placeholder' => 'Enter title',
                'name' => 'particles.owlcarousel.title'
            ],
            'particles.owlcarousel.nav' => [
                'type' => 'select.select',
                'label' => 'Prev / Next',
                'description' => 'Enable or disable the Prev / Next navigation.',
                'default' => 'disable',
                'options' => [
                    'enable' => 'Enable',
                    'disable' => 'Disable'
                ],
                'name' => 'particles.owlcarousel.nav'
            ],
            'particles.owlcarousel.dots' => [
                'type' => 'select.select',
                'label' => 'Dots',
                'description' => 'Enable or disable the Dots navigation.',
                'default' => 'enable',
                'options' => [
                    'enable' => 'Enable',
                    'disable' => 'Disable'
                ],
                'name' => 'particles.owlcarousel.dots'
            ],
            'particles.owlcarousel.autoplay' => [
                'type' => 'select.select',
                'label' => 'Autoplay',
                'description' => 'Enable or disable the Autoplay.',
                'default' => 'disable',
                'options' => [
                    'enable' => 'Enable',
                    'disable' => 'Disable'
                ],
                'name' => 'particles.owlcarousel.autoplay'
            ],
            'particles.owlcarousel.autoplaySpeed' => [
                'type' => 'input.text',
                'label' => 'Autoplay Speed',
                'description' => 'Set the speed of the Autoplay, in milliseconds.',
                'placeholder' => 5000,
                'name' => 'particles.owlcarousel.autoplaySpeed'
            ],
            'particles.owlcarousel.imageOverlay' => [
                'type' => 'select.select',
                'label' => 'Image Overlay',
                'description' => 'Enable or disable the image overlay.',
                'default' => 'enable',
                'options' => [
                    'enable' => 'Enable',
                    'disable' => 'Disable'
                ],
                'name' => 'particles.owlcarousel.imageOverlay'
            ],
            'particles.owlcarousel.items' => [
                'type' => 'collection.list',
                'array' => true,
                'label' => 'Owl Carousel Items',
                'description' => 'Create each Owl Carousel item to display.',
                'value' => 'name',
                'ajax' => true,
                'name' => 'particles.owlcarousel.items'
            ],
            'particles.owlcarousel.items.*' => [
                'type' => '_parent',
                'name' => 'particles.owlcarousel.items.*',
                'form_field' => false
            ],
            'particles.owlcarousel.items.*.image' => [
                'type' => 'input.imagepicker',
                'label' => 'Image',
                'description' => 'Select desired image.',
                'name' => 'particles.owlcarousel.items.*.image'
            ],
            'particles.owlcarousel.items.*.title' => [
                'type' => 'input.text',
                'label' => 'Title',
                'description' => 'Enter the title',
                'name' => 'particles.owlcarousel.items.*.title'
            ],
            'particles.owlcarousel.items.*.desc' => [
                'type' => 'textarea.textarea',
                'label' => 'Description',
                'description' => 'Customize the description.',
                'placeholder' => 'Enter short description',
                'name' => 'particles.owlcarousel.items.*.desc'
            ],
            'particles.owlcarousel.items.*.link' => [
                'type' => 'input.text',
                'label' => 'Link',
                'description' => 'Input the item link.',
                'name' => 'particles.owlcarousel.items.*.link'
            ],
            'particles.owlcarousel.items.*.linktext' => [
                'type' => 'input.text',
                'label' => 'Link Text',
                'description' => 'Input the text for the item link.',
                'name' => 'particles.owlcarousel.items.*.linktext'
            ],
            'particles.owlcarousel.items.*.buttonclass' => [
                'type' => 'input.text',
                'label' => 'Button Class',
                'description' => 'Input the button class.',
                'default' => 'button-outline',
                'name' => 'particles.owlcarousel.items.*.buttonclass'
            ]
        ],
        'rules' => [
            'slug' => [
                'pattern' => '[a-z][a-z0-9_\\-]+',
                'min' => 2,
                'max' => 80
            ]
        ],
        'nested' => [
            'page' => [
                'assets' => [
                    'favicon' => 'page.assets.favicon',
                    'touchicon' => 'page.assets.touchicon',
                    'css' => [
                        '*' => [
                            'name' => 'page.assets.css.*.name',
                            'location' => 'page.assets.css.*.location',
                            'inline' => 'page.assets.css.*.inline'
                        ]
                    ],
                    '_info' => 'page.assets._info',
                    'extra' => 'page.assets.extra',
                    'priority' => 'page.assets.priority',
                    'javascript' => [
                        '*' => [
                            'name' => 'page.assets.javascript.*.name',
                            'location' => 'page.assets.javascript.*.location',
                            'inline' => 'page.assets.javascript.*.inline',
                            'in_footer' => 'page.assets.javascript.*.in_footer',
                            'extra' => 'page.assets.javascript.*.extra',
                            'priority' => 'page.assets.javascript.*.priority'
                        ]
                    ]
                ],
                'body' => [
                    'doctype' => 'page.body.doctype',
                    'attribs' => [
                        'id' => 'page.body.attribs.id',
                        'class' => 'page.body.attribs.class',
                        'extra' => 'page.body.attribs.extra'
                    ],
                    'layout' => [
                        'sections' => 'page.body.layout.sections'
                    ],
                    'body_top' => 'page.body.body_top',
                    'body_bottom' => 'page.body.body_bottom'
                ],
                'head' => [
                    'meta' => 'page.head.meta',
                    'head_bottom' => 'page.head.head_bottom',
                    'atoms' => 'page.head.atoms'
                ]
            ],
            'pages' => [
                'blog_item' => [
                    'tabs' => 'pages.blog_item.tabs',
                    'blog' => 'pages.blog_item.blog',
                    'header_image' => 'pages.blog_item.header_image',
                    'header' => [
                        'header_image' => 'pages.blog_item.header.header_image',
                        'header_image_file' => 'pages.blog_item.header.header_image_file',
                        'header_image_width' => 'pages.blog_item.header.header_image_width',
                        'header_image_height' => 'pages.blog_item.header.header_image_height',
                        'summary' => [
                            'enabled' => 'pages.blog_item.header.summary.enabled',
                            'format' => 'pages.blog_item.header.summary.format',
                            'size' => 'pages.blog_item.header.summary.size',
                            'delimiter' => 'pages.blog_item.header.summary.delimiter'
                        ]
                    ],
                    'summary' => 'pages.blog_item.summary'
                ],
                'blog_list' => [
                    'tabs' => 'pages.blog_list.tabs',
                    'advanced' => 'pages.blog_list.advanced',
                    'overrides' => 'pages.blog_list.overrides',
                    'header' => [
                        'child_type' => 'pages.blog_list.header.child_type',
                        'content' => [
                            'items' => 'pages.blog_list.header.content.items',
                            'limit' => 'pages.blog_list.header.content.limit',
                            'order' => [
                                'by' => 'pages.blog_list.header.content.order.by',
                                'dir' => 'pages.blog_list.header.content.order.dir'
                            ],
                            'pagination' => 'pages.blog_list.header.content.pagination',
                            'url_taxonomy_filters' => 'pages.blog_list.header.content.url_taxonomy_filters'
                        ]
                    ],
                    'blog' => 'pages.blog_list.blog'
                ],
                'form' => 'pages.form'
            ],
            'blog_item' => [
                'tabs' => 'blog_item.tabs',
                'blog' => 'blog_item.blog',
                'header_image' => 'blog_item.header_image',
                'header' => [
                    'header_image' => 'blog_item.header.header_image',
                    'header_image_file' => 'blog_item.header.header_image_file',
                    'header_image_width' => 'blog_item.header.header_image_width',
                    'header_image_height' => 'blog_item.header.header_image_height',
                    'summary' => [
                        'enabled' => 'blog_item.header.summary.enabled',
                        'format' => 'blog_item.header.summary.format',
                        'size' => 'blog_item.header.summary.size',
                        'delimiter' => 'blog_item.header.summary.delimiter'
                    ]
                ],
                'summary' => 'blog_item.summary'
            ],
            'blog_list' => [
                'tabs' => 'blog_list.tabs',
                'advanced' => 'blog_list.advanced',
                'overrides' => 'blog_list.overrides',
                'header' => [
                    'child_type' => 'blog_list.header.child_type',
                    'content' => [
                        'items' => 'blog_list.header.content.items',
                        'limit' => 'blog_list.header.content.limit',
                        'order' => [
                            'by' => 'blog_list.header.content.order.by',
                            'dir' => 'blog_list.header.content.order.dir'
                        ],
                        'pagination' => 'blog_list.header.content.pagination',
                        'url_taxonomy_filters' => 'blog_list.header.content.url_taxonomy_filters'
                    ]
                ],
                'blog' => 'blog_list.blog'
            ],
            'default' => [
                'tabs' => 'default.tabs',
                'advanced' => 'default.advanced',
                'overrides' => 'default.overrides',
                'header' => [
                    'child_type' => 'default.header.child_type',
                    'content' => [
                        'items' => 'default.header.content.items',
                        'limit' => 'default.header.content.limit',
                        'order' => [
                            'by' => 'default.header.content.order.by',
                            'dir' => 'default.header.content.order.dir'
                        ],
                        'pagination' => 'default.header.content.pagination',
                        'url_taxonomy_filters' => 'default.header.content.url_taxonomy_filters'
                    ]
                ],
                'blog' => 'default.blog'
            ],
            'styles' => [
                'above' => [
                    'background' => 'styles.above.background',
                    'text-color' => 'styles.above.text-color'
                ],
                'accent' => [
                    'color-1' => 'styles.accent.color-1',
                    'color-2' => 'styles.accent.color-2'
                ],
                'base' => [
                    'background' => 'styles.base.background',
                    'text-color' => 'styles.base.text-color',
                    'favicon' => 'styles.base.favicon'
                ],
                'breakpoints' => [
                    'large-desktop-container' => 'styles.breakpoints.large-desktop-container',
                    'desktop-container' => 'styles.breakpoints.desktop-container',
                    'tablet-container' => 'styles.breakpoints.tablet-container',
                    'large-mobile-container' => 'styles.breakpoints.large-mobile-container',
                    'mobile-menu-breakpoint' => 'styles.breakpoints.mobile-menu-breakpoint'
                ],
                'expanded' => [
                    'background' => 'styles.expanded.background',
                    'text-color' => 'styles.expanded.text-color'
                ],
                'features' => [
                    'background' => 'styles.features.background',
                    'text-color' => 'styles.features.text-color'
                ],
                'font' => [
                    'family-default' => 'styles.font.family-default',
                    'family-title' => 'styles.font.family-title'
                ],
                'footer' => [
                    'background' => 'styles.footer.background',
                    'text-color' => 'styles.footer.text-color'
                ],
                'header' => [
                    'background' => 'styles.header.background',
                    'background-image' => 'styles.header.background-image',
                    'background-overlay' => 'styles.header.background-overlay',
                    'text-color' => 'styles.header.text-color'
                ],
                'intro' => [
                    'background' => 'styles.intro.background',
                    'text-color' => 'styles.intro.text-color'
                ],
                'menu' => [
                    'col-width' => 'styles.menu.col-width',
                    'animation' => 'styles.menu.animation'
                ],
                'navigation' => [
                    'background' => 'styles.navigation.background',
                    'text-color' => 'styles.navigation.text-color'
                ],
                'offcanvas' => [
                    'background' => 'styles.offcanvas.background',
                    'text-color' => 'styles.offcanvas.text-color',
                    'toggle-color' => 'styles.offcanvas.toggle-color',
                    'toggle-visibility' => 'styles.offcanvas.toggle-visibility',
                    'width' => 'styles.offcanvas.width'
                ],
                'testimonials' => [
                    'background' => 'styles.testimonials.background',
                    'background-image' => 'styles.testimonials.background-image',
                    'background-overlay' => 'styles.testimonials.background-overlay',
                    'text-color' => 'styles.testimonials.text-color'
                ],
                'utility' => [
                    'background' => 'styles.utility.background',
                    'text-color' => 'styles.utility.text-color'
                ]
            ],
            'particles' => [
                'analytics' => [
                    'enabled' => 'particles.analytics.enabled',
                    'ua' => [
                        'code' => 'particles.analytics.ua.code',
                        'anonym' => 'particles.analytics.ua.anonym',
                        'ssl' => 'particles.analytics.ua.ssl',
                        'debug' => 'particles.analytics.ua.debug'
                    ]
                ],
                'assets' => [
                    'enabled' => 'particles.assets.enabled',
                    'css' => [
                        '*' => [
                            'name' => 'particles.assets.css.*.name',
                            'location' => 'particles.assets.css.*.location',
                            'inline' => 'particles.assets.css.*.inline'
                        ]
                    ],
                    '_info' => 'particles.assets._info',
                    'extra' => 'particles.assets.extra',
                    'priority' => 'particles.assets.priority',
                    'javascript' => [
                        '*' => [
                            'name' => 'particles.assets.javascript.*.name',
                            'location' => 'particles.assets.javascript.*.location',
                            'inline' => 'particles.assets.javascript.*.inline',
                            'in_footer' => 'particles.assets.javascript.*.in_footer',
                            'extra' => 'particles.assets.javascript.*.extra',
                            'priority' => 'particles.assets.javascript.*.priority'
                        ]
                    ]
                ],
                'branding' => [
                    'enabled' => 'particles.branding.enabled',
                    'content' => 'particles.branding.content',
                    'css' => [
                        'class' => 'particles.branding.css.class'
                    ]
                ],
                'content' => [
                    'enabled' => 'particles.content.enabled',
                    '_info' => 'particles.content._info',
                    '_alert' => 'particles.content._alert'
                ],
                'copyright' => [
                    'enabled' => 'particles.copyright.enabled',
                    'date' => [
                        'start' => 'particles.copyright.date.start',
                        'end' => 'particles.copyright.date.end'
                    ],
                    'owner' => 'particles.copyright.owner',
                    'link' => 'particles.copyright.link',
                    'target' => 'particles.copyright.target',
                    'additional' => [
                        'text' => 'particles.copyright.additional.text'
                    ],
                    'css' => [
                        'class' => 'particles.copyright.css.class'
                    ]
                ],
                'custom' => [
                    'enabled' => 'particles.custom.enabled',
                    'html' => 'particles.custom.html',
                    'twig' => 'particles.custom.twig',
                    'filter' => 'particles.custom.filter'
                ],
                'date' => [
                    'enabled' => 'particles.date.enabled',
                    'css' => [
                        'class' => 'particles.date.css.class'
                    ],
                    'date' => [
                        'formats' => 'particles.date.date.formats'
                    ]
                ],
                'logo' => [
                    'enabled' => 'particles.logo.enabled',
                    'url' => 'particles.logo.url',
                    'image' => 'particles.logo.image',
                    'svg' => 'particles.logo.svg',
                    'text' => 'particles.logo.text',
                    'class' => 'particles.logo.class'
                ],
                'menu' => [
                    'enabled' => 'particles.menu.enabled',
                    '_info' => 'particles.menu._info',
                    'menu' => 'particles.menu.menu',
                    'base' => 'particles.menu.base',
                    'startLevel' => 'particles.menu.startLevel',
                    'maxLevels' => 'particles.menu.maxLevels',
                    'renderTitles' => 'particles.menu.renderTitles',
                    'hoverExpand' => 'particles.menu.hoverExpand',
                    'mobileTarget' => 'particles.menu.mobileTarget'
                ],
                'messages' => [
                    'enabled' => 'particles.messages.enabled',
                    '_info' => 'particles.messages._info',
                    '_alert' => 'particles.messages._alert'
                ],
                'mobile-menu' => [
                    'enabled' => 'particles.mobile-menu.enabled',
                    '_note' => 'particles.mobile-menu._note'
                ],
                'position' => [
                    'enabled' => 'particles.position.enabled',
                    'key' => 'particles.position.key',
                    'chrome' => 'particles.position.chrome'
                ],
                'social' => [
                    'enabled' => 'particles.social.enabled',
                    'css' => [
                        'class' => 'particles.social.css.class'
                    ],
                    'title' => 'particles.social.title',
                    'target' => 'particles.social.target',
                    'items' => [
                        '*' => [
                            'name' => 'particles.social.items.*.name',
                            'icon' => 'particles.social.items.*.icon',
                            'text' => 'particles.social.items.*.text',
                            'link' => 'particles.social.items.*.link'
                        ]
                    ]
                ],
                'spacer' => [
                    'enabled' => 'particles.spacer.enabled',
                    '_info' => 'particles.spacer._info'
                ],
                'totop' => [
                    'enabled' => 'particles.totop.enabled',
                    'css' => [
                        'class' => 'particles.totop.css.class'
                    ],
                    'icon' => 'particles.totop.icon',
                    'content' => 'particles.totop.content'
                ],
                'contentcubes' => [
                    'enabled' => 'particles.contentcubes.enabled',
                    'css' => [
                        'class' => 'particles.contentcubes.css.class'
                    ],
                    'title' => 'particles.contentcubes.title',
                    'items' => [
                        '*' => [
                            'image' => 'particles.contentcubes.items.*.image',
                            'imageposition' => 'particles.contentcubes.items.*.imageposition',
                            'label' => 'particles.contentcubes.items.*.label',
                            'title' => 'particles.contentcubes.items.*.title',
                            'tags' => [
                                '*' => [
                                    'text' => 'particles.contentcubes.items.*.tags.*.text',
                                    'icon' => 'particles.contentcubes.items.*.tags.*.icon',
                                    'link' => 'particles.contentcubes.items.*.tags.*.link',
                                    'target' => 'particles.contentcubes.items.*.tags.*.target'
                                ]
                            ],
                            'link' => 'particles.contentcubes.items.*.link',
                            'linktext' => 'particles.contentcubes.items.*.linktext',
                            'buttonclass' => 'particles.contentcubes.items.*.buttonclass',
                            'buttontarget' => 'particles.contentcubes.items.*.buttontarget'
                        ]
                    ]
                ],
                'contenttabs' => [
                    'enabled' => 'particles.contenttabs.enabled',
                    'class' => 'particles.contenttabs.class',
                    'title' => 'particles.contenttabs.title',
                    'animation' => 'particles.contenttabs.animation',
                    'items' => [
                        '*' => [
                            'title' => 'particles.contenttabs.items.*.title',
                            'content' => 'particles.contenttabs.items.*.content'
                        ]
                    ]
                ],
                'horizontalmenu' => [
                    'enabled' => 'particles.horizontalmenu.enabled',
                    'class' => 'particles.horizontalmenu.class',
                    'target' => 'particles.horizontalmenu.target',
                    'items' => [
                        '*' => [
                            'name' => 'particles.horizontalmenu.items.*.name',
                            'text' => 'particles.horizontalmenu.items.*.text',
                            'link' => 'particles.horizontalmenu.items.*.link'
                        ]
                    ]
                ],
                'owlcarousel' => [
                    'enabled' => 'particles.owlcarousel.enabled',
                    'class' => 'particles.owlcarousel.class',
                    'title' => 'particles.owlcarousel.title',
                    'nav' => 'particles.owlcarousel.nav',
                    'dots' => 'particles.owlcarousel.dots',
                    'autoplay' => 'particles.owlcarousel.autoplay',
                    'autoplaySpeed' => 'particles.owlcarousel.autoplaySpeed',
                    'imageOverlay' => 'particles.owlcarousel.imageOverlay',
                    'items' => [
                        '*' => [
                            'image' => 'particles.owlcarousel.items.*.image',
                            'title' => 'particles.owlcarousel.items.*.title',
                            'desc' => 'particles.owlcarousel.items.*.desc',
                            'link' => 'particles.owlcarousel.items.*.link',
                            'linktext' => 'particles.owlcarousel.items.*.linktext',
                            'buttonclass' => 'particles.owlcarousel.items.*.buttonclass'
                        ]
                    ]
                ]
            ]
        ],
        'dynamic' => [
            'blog_item.header.header_image' => [
                '' => [
                    'action' => 'replace',
                    'params' => true
                ]
            ],
            'blog_item.header.summary.enabled' => [
                '' => [
                    'action' => 'replace',
                    'params' => true
                ]
            ]
        ],
        'filter' => [
            'validation' => true
        ]
    ]
];
