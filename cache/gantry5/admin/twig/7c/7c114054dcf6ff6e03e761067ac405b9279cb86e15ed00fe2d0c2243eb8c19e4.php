<?php

/* @gantry-admin/partials/php_unsupported.html.twig */
class __TwigTemplate_10518988546234fe26945a84d8901419559ef636c8fb61b7ba26203b79b3718d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $context["php_version"] = twig_constant("PHP_VERSION");
        // line 2
        echo "
";
        // line 3
        if ((is_string($__internal_000869199d7378a1a8bc5cafb7d2366666c4a72a7e9bc491fb8690fa1f631ae4 = (isset($context["php_version"]) ? $context["php_version"] : null)) && is_string($__internal_0be1dbd5079e2287d0df40ce78fc1a555c44df3d80b0bf9e4b9f066c6574527b = "5.4") && ('' === $__internal_0be1dbd5079e2287d0df40ce78fc1a555c44df3d80b0bf9e4b9f066c6574527b || 0 === strpos($__internal_000869199d7378a1a8bc5cafb7d2366666c4a72a7e9bc491fb8690fa1f631ae4, $__internal_0be1dbd5079e2287d0df40ce78fc1a555c44df3d80b0bf9e4b9f066c6574527b)))) {
            // line 4
            echo "<div class=\"g-grid\">
    <div class=\"g-block alert alert-warning g-php-outdated\">
        ";
            // line 6
            echo $this->env->getExtension('GantryTwig')->transFilter("GANTRY5_PLATFORM_PHP54_WARNING", (isset($context["php_version"]) ? $context["php_version"] : null));
            echo "
    </div>
</div>
";
        }
    }

    public function getTemplateName()
    {
        return "@gantry-admin/partials/php_unsupported.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  30 => 6,  26 => 4,  24 => 3,  21 => 2,  19 => 1,);
    }
}
/* {% set php_version = constant('PHP_VERSION') %}*/
/* */
/* {% if php_version starts with '5.4' %}*/
/* <div class="g-grid">*/
/*     <div class="g-block alert alert-warning g-php-outdated">*/
/*         {{ 'GANTRY5_PLATFORM_PHP54_WARNING'|trans(php_version)|raw }}*/
/*     </div>*/
/* </div>*/
/* {% endif %}*/
