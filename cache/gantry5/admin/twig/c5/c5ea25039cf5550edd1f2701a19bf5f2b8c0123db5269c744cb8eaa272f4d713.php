<?php

/* forms/fields/collection/list.html.twig */
class __TwigTemplate_e5dc57d22d345c79b5cecd50379a6aacab7cde2f1c600171ed442ac2db79fae5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'field' => array($this, 'block_field'),
            'contents' => array($this, 'block_contents'),
            'label' => array($this, 'block_label'),
            'input' => array($this, 'block_input'),
            'collection_fields' => array($this, 'block_collection_fields'),
        );
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return $this->loadTemplate((("forms/" . ((array_key_exists("layout", $context)) ? (_twig_default_filter((isset($context["layout"]) ? $context["layout"] : null), "field")) : ("field"))) . ".html.twig"), "forms/fields/collection/list.html.twig", 1);
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 4
        $context["value"] = (((( !$this->getAttribute((isset($context["field"]) ? $context["field"] : null), "key", array()) && twig_test_iterable((isset($context["value"]) ? $context["value"] : null))) && twig_length_filter($this->env, (isset($context["value"]) ? $context["value"] : null)))) ? ($this->env->getExtension('GantryTwig')->valuesFilter((isset($context["value"]) ? $context["value"] : null))) : ((isset($context["value"]) ? $context["value"] : null)));
        // line 1
        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
    }

    // line 6
    public function block_field($context, array $blocks = array())
    {
        // line 7
        echo "    ";
        if ($this->getAttribute((isset($context["field"]) ? $context["field"] : null), "is_current", array())) {
            // line 8
            echo "        <div class=\"g-grid\">
            ";
            // line 9
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["value"]) ? $context["value"] : null));
            $context['loop'] = array(
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            );
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["key"] => $context["val"]) {
                // line 10
                echo "                ";
                $context["title"] = ((($this->getAttribute((isset($context["field"]) ? $context["field"] : null), "value", array()) == $this->getAttribute((isset($context["field"]) ? $context["field"] : null), "key", array()))) ? ($context["key"]) : ($this->getAttribute($context["val"], $this->getAttribute((isset($context["field"]) ? $context["field"] : null), "value", array()), array(), "array")));
                // line 11
                echo "                <div class=\"card settings-block\">
                    <h4>
                        <span data-title-editable=\"";
                // line 13
                echo twig_escape_filter($this->env, (isset($context["title"]) ? $context["title"] : null), "html", null, true);
                echo "\" data-collection-key=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('GantryTwig')->fieldNameFilter((((((isset($context["scope"]) ? $context["scope"] : null) . ".") . $context["key"]) . ".") . $this->getAttribute((isset($context["field"]) ? $context["field"] : null), "value", array()))), "html", null, true);
                echo "\" class=\"title\">";
                echo twig_escape_filter($this->env, (isset($context["title"]) ? $context["title"] : null), "html", null, true);
                echo "</span>
                        <i class=\"fa fa-pencil font-small\"  tabindex=\"0\" aria-label=\"";
                // line 14
                echo twig_escape_filter($this->env, twig_replace_filter($this->env->getExtension('GantryTwig')->transFilter("GANTRY5_PLATFORM_EDIT_TITLE"), array("%s" => (isset($context["title"]) ? $context["title"] : null))), "html", null, true);
                echo "\" data-title-edit=\"\"></i>
                    </h4>
                    <div class=\"inner-params\">
                        ";
                // line 17
                $this->displayBlock("collection_fields", $context, $blocks);
                echo "
                    </div>
                </div>
            ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['key'], $context['val'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 21
            echo "        </div>
    ";
        } else {
            // line 23
            echo "        ";
            $context["can_reorder"] = ((($this->getAttribute((isset($context["field"]) ? $context["field"] : null), "reorder", array(), "any", true, true) &&  !(null === $this->getAttribute((isset($context["field"]) ? $context["field"] : null), "reorder", array())))) ? ($this->getAttribute((isset($context["field"]) ? $context["field"] : null), "reorder", array())) : (true));
            // line 24
            echo "        ";
            $context["can_remove"] = ((($this->getAttribute((isset($context["field"]) ? $context["field"] : null), "deletion", array(), "any", true, true) &&  !(null === $this->getAttribute((isset($context["field"]) ? $context["field"] : null), "deletion", array())))) ? ($this->getAttribute((isset($context["field"]) ? $context["field"] : null), "deletion", array())) : (true));
            // line 25
            echo "        ";
            $context["can_addnew"] = ((($this->getAttribute((isset($context["field"]) ? $context["field"] : null), "add_new", array(), "any", true, true) &&  !(null === $this->getAttribute((isset($context["field"]) ? $context["field"] : null), "add_new", array())))) ? ($this->getAttribute((isset($context["field"]) ? $context["field"] : null), "add_new", array())) : (true));
            // line 26
            echo "        <div class=\"settings-param ";
            echo twig_escape_filter($this->env, twig_replace_filter($this->getAttribute((isset($context["field"]) ? $context["field"] : null), "type", array()), ".", "-"), "html", null, true);
            echo "\">
            ";
            // line 27
            if ((((isset($context["overrideable"]) ? $context["overrideable"] : null) && (( !$this->getAttribute((isset($context["field"]) ? $context["field"] : null), "overridable", array(), "any", true, true) || ($this->getAttribute((isset($context["field"]) ? $context["field"] : null), "overridable", array()) == true)) || (isset($context["has_value"]) ? $context["has_value"] : null))) && ($this->getAttribute((isset($context["field"]) ? $context["field"] : null), "type", array()) != "container.set"))) {
                // line 28
                echo "                ";
                $this->loadTemplate("forms/override.html.twig", "forms/fields/collection/list.html.twig", 28)->display(array_merge($context, array("scope" => (isset($context["scope"]) ? $context["scope"] : null), "name" => (isset($context["name"]) ? $context["name"] : null), "field" => (isset($context["field"]) ? $context["field"] : null))));
                // line 29
                echo "            ";
            }
            // line 30
            echo "            ";
            $this->displayBlock('contents', $context, $blocks);
            // line 104
            echo "        </div>
    ";
        }
    }

    // line 30
    public function block_contents($context, array $blocks = array())
    {
        // line 31
        echo "                ";
        $context["field_route"] = twig_replace_filter((((((isset($context["route"]) ? $context["route"] : null) . ".") . (isset($context["prefix"]) ? $context["prefix"] : null)) . ".") . $this->getAttribute((isset($context["field"]) ? $context["field"] : null), "name", array())), ".", "/");
        // line 32
        echo "                <span class=\"settings-param-title float-left\">
                    ";
        // line 33
        $this->displayBlock('label', $context, $blocks);
        // line 41
        echo "                </span>
                <div class=\"settings-param-field\" data-field-name=\"";
        // line 42
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["field"]) ? $context["field"] : null), "name", array()), "html", null, true);
        echo "\">
                    ";
        // line 43
        $this->displayBlock('input', $context, $blocks);
        // line 102
        echo "                </div>
            ";
    }

    // line 33
    public function block_label($context, array $blocks = array())
    {
        // line 34
        echo "                        ";
        if ($this->getAttribute((isset($context["field"]) ? $context["field"] : null), "description", array())) {
            // line 35
            echo "                            <span aria-label=\"";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["field"]) ? $context["field"] : null), "description", array()), "html", null, true);
            echo "\" data-tip=\"";
            echo $this->getAttribute((isset($context["field"]) ? $context["field"] : null), "description", array());
            echo "\" data-tip-place=\"top-right\">";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["field"]) ? $context["field"] : null), "label", array()), "html", null, true);
            echo "</span>
                        ";
        } else {
            // line 37
            echo "                            ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["field"]) ? $context["field"] : null), "label", array()), "html", null, true);
            echo "
                        ";
        }
        // line 39
        echo "                        ";
        echo ((twig_in_filter($this->getAttribute($this->getAttribute((isset($context["field"]) ? $context["field"] : null), "validate", array()), "required", array()), array(0 => "on", 1 => "true", 2 => 1))) ? ("<span class=\"required\">*</span>") : (""));
        echo "
                    ";
    }

    // line 43
    public function block_input($context, array $blocks = array())
    {
        // line 44
        echo "<div class=\"g5-collection-wrapper\">
                        <ul>";
        // line 46
        if ($this->getAttribute((isset($context["field"]) ? $context["field"] : null), "fields", array())) {
            // line 47
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["value"]) ? $context["value"] : null));
            $context['loop'] = array(
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            );
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["key"] => $context["val"]) {
                // line 48
                echo "                                    ";
                if (($this->getAttribute((isset($context["field"]) ? $context["field"] : null), "ajax", array()) == true)) {
                    // line 49
                    echo "                                        <li data-collection-item=\"";
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["field"]) ? $context["field"] : null), "value", array()), "html", null, true);
                    echo "\">
                                            ";
                    // line 50
                    $context["itemValue"] = ((($this->getAttribute((isset($context["field"]) ? $context["field"] : null), "value", array()) == $this->getAttribute((isset($context["field"]) ? $context["field"] : null), "key", array()))) ? ($context["key"]) : ($this->getAttribute($context["val"], $this->getAttribute((isset($context["field"]) ? $context["field"] : null), "value", array()), array(), "array")));
                    // line 51
                    echo "                                            ";
                    if ((isset($context["can_reorder"]) ? $context["can_reorder"] : null)) {
                        echo "<i class=\"fa fa-reorder font-small item-reorder\"></i>";
                    }
                    // line 52
                    echo "                                            <a class=\"config-cog\" href=\"";
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["gantry"]) ? $context["gantry"] : null), "route", array(0 => (((isset($context["field_route"]) ? $context["field_route"] : null) . "/") . $context["key"])), "method"), "html", null, true);
                    echo "\"><span data-title-editable=\"";
                    echo twig_escape_filter($this->env, (isset($context["itemValue"]) ? $context["itemValue"] : null), "html", null, true);
                    echo "\" class=\"title\">";
                    echo twig_escape_filter($this->env, (isset($context["itemValue"]) ? $context["itemValue"] : null), "html", null, true);
                    echo "</span></a>
                                            ";
                    // line 53
                    if ((isset($context["can_remove"]) ? $context["can_remove"] : null)) {
                        echo "<i class=\"fa fa-fw fa-trash font-small\" data-collection-remove=\"\"></i>";
                    }
                    // line 54
                    echo "                                            ";
                    if ((isset($context["can_addnew"]) ? $context["can_addnew"] : null)) {
                        echo "<i class=\"fa fa-files-o font-small\" data-collection-duplicate=\"\"></i>";
                    }
                    // line 55
                    echo "                                            <i class=\"fa fa-fw fa-pencil font-small\" tabindex=\"0\" aria-label=\"";
                    echo twig_escape_filter($this->env, $this->env->getExtension('GantryTwig')->transFilter("GANTRY5_PLATFORM_EDIT_TITLE", (isset($context["itemValue"]) ? $context["itemValue"] : null)), "html", null, true);
                    echo "\" data-title-edit=\"\"></i>
                                        </li>
                                    ";
                } else {
                    // line 58
                    echo "                                        ";
                    $this->displayBlock('collection_fields', $context, $blocks);
                    // line 82
                    echo "                                    ";
                }
                // line 83
                echo "                                ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['key'], $context['val'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
        }
        // line 85
        echo "</ul>
                    </div>
                    <div>
                        <ul style=\"display: none\">
                            <li data-collection-nosort=\"\" data-collection-template=\"";
        // line 89
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["field"]) ? $context["field"] : null), "value", array()), "html", null, true);
        echo "\" style=\"display: none;\">
                                ";
        // line 90
        if ((isset($context["can_reorder"]) ? $context["can_reorder"] : null)) {
            echo "<i class=\"fa fa-reorder font-small item-reorder\"></i>";
        }
        // line 91
        echo "                                <a class=\"config-cog\" href=\"";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["gantry"]) ? $context["gantry"] : null), "route", array(0 => ((isset($context["field_route"]) ? $context["field_route"] : null) . "/%id%")), "method"), "html", null, true);
        echo "\"><span data-title-editable=\"New item\" class=\"title\">New item</span></a>
                                ";
        // line 92
        if ((isset($context["can_remove"]) ? $context["can_remove"] : null)) {
            echo "<i class=\"fa fa-fw fa-trash font-small\" data-collection-remove=\"\"></i>";
        }
        // line 93
        echo "                                ";
        if ((isset($context["can_addnew"]) ? $context["can_addnew"] : null)) {
            echo "<i class=\"fa fa-files-o font-small\" data-collection-duplicate=\"\"></i>";
        }
        // line 94
        echo "                                <i class=\"fa fa-fw fa-pencil font-small\" data-title-edit=\"\"></i>
                            </li>
                        </ul>
                        ";
        // line 97
        if ((isset($context["can_addnew"]) ? $context["can_addnew"] : null)) {
            echo "<span class=\"collection-addnew button button-simple\" data-collection-addnew=\"\" title=\"Add new item\"><i class=\"fa fa-plus font-small\"></i></span>";
        }
        // line 98
        echo "                        <a href=\"";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["gantry"]) ? $context["gantry"] : null), "route", array(0 => (isset($context["field_route"]) ? $context["field_route"] : null)), "method"), "html", null, true);
        echo "\" class=\"collection-editall button button-simple\" data-collection-editall=\"\" title=\"Edit all items\" ";
        if ((twig_length_filter($this->env, (isset($context["value"]) ? $context["value"] : null)) <= 1)) {
            echo "style=\"display: none;\"";
        }
        echo "><i class=\"fa fa-th-large font-small\"></i></a>
                    </div>
                    <input data-collection-data=\"\" name=\"";
        // line 100
        echo twig_escape_filter($this->env, $this->env->getExtension('GantryTwig')->fieldNameFilter((((isset($context["scope"]) ? $context["scope"] : null) . (isset($context["name"]) ? $context["name"] : null)) . "._json")), "html", null, true);
        echo "\" type=\"hidden\" value=\"";
        echo twig_escape_filter($this->env, twig_jsonencode_filter(((array_key_exists("value", $context)) ? (_twig_default_filter((isset($context["value"]) ? $context["value"] : null), array())) : (array())), twig_constant("JSON_UNESCAPED_SLASHES")), "html_attr");
        echo "\"/>
                    ";
    }

    // line 58
    public function block_collection_fields($context, array $blocks = array())
    {
        // line 59
        echo "                                            <div data-g5-collections=\"\">
                                                ";
        // line 60
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["field"]) ? $context["field"] : null), "fields", array()));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["childName"] => $context["child"]) {
            // line 61
            echo "                                                    ";
            if ((is_string($__internal_18126d84e63d89dddc4c606e406d23c55378de45084b462b2fda082f09ece8fd = $context["childName"]) && is_string($__internal_e7c69c46bf67cb71bf442e52450636e5b48c72ec2ce2225d190af2fc0dc5118a = ".") && ('' === $__internal_e7c69c46bf67cb71bf442e52450636e5b48c72ec2ce2225d190af2fc0dc5118a || 0 === strpos($__internal_18126d84e63d89dddc4c606e406d23c55378de45084b462b2fda082f09ece8fd, $__internal_e7c69c46bf67cb71bf442e52450636e5b48c72ec2ce2225d190af2fc0dc5118a)))) {
                // line 62
                echo "                                                        ";
                $context["childKey"] = trim($context["childName"], ".");
                // line 63
                echo "                                                        ";
                $context["childValue"] = $this->getAttribute((isset($context["val"]) ? $context["val"] : null), twig_slice($this->env, $context["childName"], 1, null), array(), "array");
                // line 64
                echo "                                                        ";
                $context["childName"] = ((((isset($context["name"]) ? $context["name"] : null) . ".") . (isset($context["key"]) ? $context["key"] : null)) . $context["childName"]);
                // line 65
                echo "                                                    ";
            } else {
                // line 66
                echo "                                                        ";
                $context["childKey"] = $context["childName"];
                // line 67
                echo "                                                        ";
                $context["childValue"] = $this->env->getExtension('GantryTwig')->nestedFunc((isset($context["data"]) ? $context["data"] : null), ((isset($context["scope"]) ? $context["scope"] : null) . $context["childName"]));
                // line 68
                echo "                                                        ";
                $context["childName"] = twig_replace_filter($context["childName"], array("*" => (isset($context["key"]) ? $context["key"] : null)));
                // line 69
                echo "                                                    ";
            }
            // line 70
            echo "                                                    ";
            if (((!twig_in_filter($context["childName"], (isset($context["skip"]) ? $context["skip"] : null)) &&  !$this->getAttribute($context["child"], "skip", array())) && ($this->getAttribute((isset($context["field"]) ? $context["field"] : null), "value", array()) != (isset($context["childKey"]) ? $context["childKey"] : null)))) {
                // line 71
                echo "                                                         ";
                if (($this->getAttribute($context["child"], "type", array()) == "key")) {
                    // line 72
                    echo "                                                             ";
                    $this->loadTemplate("forms/fields/key/key.html.twig", "forms/fields/collection/list.html.twig", 72)->display(array_merge($context, array("name" =>                     // line 73
$context["childName"], "field" => $context["child"], "value" => (isset($context["key"]) ? $context["key"] : null))));
                    // line 74
                    echo "                                                         ";
                } elseif ($this->getAttribute($context["child"], "type", array())) {
                    // line 75
                    echo "                                                             ";
                    $this->loadTemplate(array(0 => (("forms/fields/" . twig_replace_filter($this->getAttribute($context["child"], "type", array()), ".", "/")) . ".html.twig"), 1 => "forms/fields/unknown/unknown.html.twig"), "forms/fields/collection/list.html.twig", 75)->display(array_merge($context, array("name" =>                     // line 76
$context["childName"], "field" => $context["child"], "current_value" => (isset($context["childValue"]) ? $context["childValue"] : null), "value" => null, "default_value" => null, "prefix" => ((((isset($context["prefix"]) ? $context["prefix"] : null)) ? (((isset($context["prefix"]) ? $context["prefix"] : null) . ".")) : ("")) . $context["childName"]))));
                    // line 77
                    echo "                                                        ";
                }
                // line 78
                echo "                                                    ";
            }
            // line 79
            echo "                                                ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['childName'], $context['child'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 80
        echo "                                            </div>
                                        ";
    }

    public function getTemplateName()
    {
        return "forms/fields/collection/list.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  416 => 80,  402 => 79,  399 => 78,  396 => 77,  394 => 76,  392 => 75,  389 => 74,  387 => 73,  385 => 72,  382 => 71,  379 => 70,  376 => 69,  373 => 68,  370 => 67,  367 => 66,  364 => 65,  361 => 64,  358 => 63,  355 => 62,  352 => 61,  335 => 60,  332 => 59,  329 => 58,  321 => 100,  311 => 98,  307 => 97,  302 => 94,  297 => 93,  293 => 92,  288 => 91,  284 => 90,  280 => 89,  274 => 85,  259 => 83,  256 => 82,  253 => 58,  246 => 55,  241 => 54,  237 => 53,  228 => 52,  223 => 51,  221 => 50,  216 => 49,  213 => 48,  196 => 47,  194 => 46,  191 => 44,  188 => 43,  181 => 39,  175 => 37,  165 => 35,  162 => 34,  159 => 33,  154 => 102,  152 => 43,  148 => 42,  145 => 41,  143 => 33,  140 => 32,  137 => 31,  134 => 30,  128 => 104,  125 => 30,  122 => 29,  119 => 28,  117 => 27,  112 => 26,  109 => 25,  106 => 24,  103 => 23,  99 => 21,  81 => 17,  75 => 14,  67 => 13,  63 => 11,  60 => 10,  43 => 9,  40 => 8,  37 => 7,  34 => 6,  30 => 1,  28 => 4,  22 => 1,);
    }
}
/* {% extends 'forms/' ~ layout|default('field') ~ '.html.twig' %}*/
/* */
/* {# If values contains a plain list of items, we need to reindex them. #}*/
/* {% set value = not field.key and value is iterable and value|length ? value|values : value %}*/
/* */
/* {% block field %}*/
/*     {% if field.is_current %}*/
/*         <div class="g-grid">*/
/*             {% for key, val in value %}*/
/*                 {% set title = (field.value == field.key ? key : val[field.value]) %}*/
/*                 <div class="card settings-block">*/
/*                     <h4>*/
/*                         <span data-title-editable="{{ title }}" data-collection-key="{{ (scope ~ '.' ~ key ~ '.' ~ field.value)|fieldName }}" class="title">{{ title }}</span>*/
/*                         <i class="fa fa-pencil font-small"  tabindex="0" aria-label="{{ 'GANTRY5_PLATFORM_EDIT_TITLE'|trans|replace({'%s': title}) }}" data-title-edit=""></i>*/
/*                     </h4>*/
/*                     <div class="inner-params">*/
/*                         {{ block('collection_fields') }}*/
/*                     </div>*/
/*                 </div>*/
/*             {% endfor %}*/
/*         </div>*/
/*     {% else %}*/
/*         {% set can_reorder = field.reorder ?? true %}*/
/*         {% set can_remove = field.deletion ?? true %}*/
/*         {% set can_addnew = field.add_new ?? true %}*/
/*         <div class="settings-param {{ field.type|replace('.', '-') }}">*/
/*             {% if overrideable and (field.overridable is not defined or field.overridable == true or has_value) and field.type != 'container.set' %}*/
/*                 {% include 'forms/override.html.twig' with {'scope': scope, 'name': name, 'field': field} %}*/
/*             {% endif %}*/
/*             {% block contents %}*/
/*                 {% set field_route = (route ~ '.' ~ prefix ~ '.' ~ field.name)|replace('.', '/') %}*/
/*                 <span class="settings-param-title float-left">*/
/*                     {% block label %}*/
/*                         {% if field.description %}*/
/*                             <span aria-label="{{ field.description }}" data-tip="{{ field.description|raw }}" data-tip-place="top-right">{{ field.label }}</span>*/
/*                         {% else %}*/
/*                             {{ field.label }}*/
/*                         {% endif %}*/
/*                         {{ field.validate.required in ['on', 'true', 1] ? '<span class="required">*</span>' }}*/
/*                     {% endblock %}*/
/*                 </span>*/
/*                 <div class="settings-param-field" data-field-name="{{ field.name }}">*/
/*                     {% block input -%}*/
/*                         <div class="g5-collection-wrapper">*/
/*                         <ul>*/
/*                         {%- if field.fields -%}*/
/*                                 {% for key, val in value %}*/
/*                                     {% if (field.ajax == true) %}*/
/*                                         <li data-collection-item="{{ field.value }}">*/
/*                                             {% set itemValue = field.value == field.key ? key : val[field.value] %}*/
/*                                             {% if can_reorder %}<i class="fa fa-reorder font-small item-reorder"></i>{% endif %}*/
/*                                             <a class="config-cog" href="{{ gantry.route(field_route ~ '/' ~ key) }}"><span data-title-editable="{{ itemValue }}" class="title">{{ itemValue }}</span></a>*/
/*                                             {% if can_remove %}<i class="fa fa-fw fa-trash font-small" data-collection-remove=""></i>{% endif %}*/
/*                                             {% if can_addnew %}<i class="fa fa-files-o font-small" data-collection-duplicate=""></i>{% endif %}*/
/*                                             <i class="fa fa-fw fa-pencil font-small" tabindex="0" aria-label="{{ 'GANTRY5_PLATFORM_EDIT_TITLE'|trans(itemValue) }}" data-title-edit=""></i>*/
/*                                         </li>*/
/*                                     {% else %}*/
/*                                         {% block collection_fields %}*/
/*                                             <div data-g5-collections="">*/
/*                                                 {% for childName, child in field.fields %}*/
/*                                                     {% if childName starts with '.' %}*/
/*                                                         {% set childKey = childName|trim('.') %}*/
/*                                                         {% set childValue = val[childName[1:]] %}*/
/*                                                         {% set childName = name ~ '.' ~ key ~ childName %}*/
/*                                                     {% else %}*/
/*                                                         {% set childKey = childName %}*/
/*                                                         {% set childValue = nested(data, scope ~ childName) %}*/
/*                                                         {% set childName = childName|replace({'*': key}) %}*/
/*                                                     {% endif %}*/
/*                                                     {% if childName not in skip and not child.skip and field.value != childKey %}*/
/*                                                          {% if child.type == 'key' %}*/
/*                                                              {% include 'forms/fields/key/key.html.twig'*/
/*                                                              with {name: childName, field: child, value: key} %}*/
/*                                                          {% elseif child.type %}*/
/*                                                              {% include ["forms/fields/" ~ child.type|replace('.', '/') ~ ".html.twig", 'forms/fields/unknown/unknown.html.twig']*/
/*                                                              with {name: childName, field: child, current_value: childValue, value: null, default_value: null, prefix: (prefix ? prefix ~ '.' : '') ~ childName} %}*/
/*                                                         {% endif %}*/
/*                                                     {% endif %}*/
/*                                                 {% endfor %}*/
/*                                             </div>*/
/*                                         {% endblock %}*/
/*                                     {% endif %}*/
/*                                 {% endfor %}*/
/*                         {%- endif -%}*/
/*                     </ul>*/
/*                     </div>*/
/*                     <div>*/
/*                         <ul style="display: none">*/
/*                             <li data-collection-nosort="" data-collection-template="{{ field.value }}" style="display: none;">*/
/*                                 {% if can_reorder %}<i class="fa fa-reorder font-small item-reorder"></i>{% endif %}*/
/*                                 <a class="config-cog" href="{{ gantry.route(field_route ~ '/%id%') }}"><span data-title-editable="New item" class="title">New item</span></a>*/
/*                                 {% if can_remove %}<i class="fa fa-fw fa-trash font-small" data-collection-remove=""></i>{% endif %}*/
/*                                 {% if can_addnew %}<i class="fa fa-files-o font-small" data-collection-duplicate=""></i>{% endif %}*/
/*                                 <i class="fa fa-fw fa-pencil font-small" data-title-edit=""></i>*/
/*                             </li>*/
/*                         </ul>*/
/*                         {% if can_addnew %}<span class="collection-addnew button button-simple" data-collection-addnew="" title="Add new item"><i class="fa fa-plus font-small"></i></span>{% endif %}*/
/*                         <a href="{{ gantry.route(field_route) }}" class="collection-editall button button-simple" data-collection-editall="" title="Edit all items" {% if value|length <= 1 %}style="display: none;"{% endif %}><i class="fa fa-th-large font-small"></i></a>*/
/*                     </div>*/
/*                     <input data-collection-data="" name="{{ (scope ~ name ~ '._json')|fieldName }}" type="hidden" value="{{ value|default({})|json_encode(constant('JSON_UNESCAPED_SLASHES'))|e('html_attr') }}"/>*/
/*                     {% endblock %}*/
/*                 </div>*/
/*             {% endblock %}*/
/*         </div>*/
/*     {% endif %}*/
/* {% endblock %}*/
/* */
