<?php
return [
    '@class' => 'Grav\\Common\\File\\CompiledYamlFile',
    'filename' => 'plugins://gantry5/gantry5.yaml',
    'modified' => 1469626730,
    'data' => [
        'enabled' => true,
        'route' => '/admin/gantry5',
        'debug' => false,
        'production' => true,
        'asset_timestamps' => true,
        'asset_timestamps_period' => 7,
        'compile_yaml' => true,
        'compile_twig' => true
    ]
];
