<?php
return [
    '@class' => 'Grav\\Common\\File\\CompiledYamlFile',
    'filename' => 'C:/xampp/htdocs/intromagang/grav/user/themes/g5_helium/blueprints.yaml',
    'modified' => 1469626738,
    'data' => [
        'name' => 'Helium',
        'version' => 'dev-651e10ba3',
        'description' => 'Default Gantry 5 theme.',
        'icon' => 'gantry',
        'author' => [
            'name' => 'RocketTheme',
            'email' => 'support@rockettheme.com',
            'url' => 'http://www.rockettheme.com'
        ],
        'homepage' => 'http://gantry.org',
        'keywords' => 'gantry, gantry5, theme',
        'bugs' => 'https://github.com/gantry/gantry5/issues',
        'license' => 'MIT',
        'form' => [
            'validation' => 'loose',
            'fields' => NULL
        ]
    ]
];
