---
title: 'Perbedaan CSS Position Static, Relative, Absolute, Fixed dan Inherit'
content:
    items: '@self.children'
    limit: 5
    order:
        by: date
        dir: desc
    pagination: true
    url_taxonomy_filters: true
---

Kali ini saya akan membahas apa sih perbedaan antara CSS Position Static, Relative, Absolute, Fixed dan Inherit.

Perlu diketahui sebelumnya, bahwa properti position memanipulasi lokasi elemen. Ada beberapa jenis position yang bisa kita tentukan, antara lain :

* Static
* Relative
* Absolute
* Fixed
* Inherit

### Static

Properti static merupakan properti bawaan semua elemen, dimana semua elemen akan memiliki properti ini. Dengan properti ini, elemen akan menempati posisi mengikuti alur halaman.

Jadi, apabila kita menambahkan value top, bottom, left, right, value tersebut tidak akan menghasilkan apapun.

### Relative

Posisi elemen tetap mengikuti alur dokumen, hampir sama dengan value static. Yang menjadi perbedaan yaitu, value top, bottom, left, right akan berfungsi. Pengaturan posisi pada suatu elemen menggunakan value tersebut akan “mendorong” elemen tersebut ke arah yang diinginkan

### Absolute

Elemen akan dihilangkan / dicabut dari alur dokumen, dan elemen lainnya akan bekerja sebagaimana mestinya (tidak terganggu dengan elemen yang diberikan properti absolute)

Apabila element yang mempunyai properti absolute tersebut ada didalam sebuah element lagi (parent), maka element tersebut akan diabaikan, seakan-akan elemen tersebut tidak ada di dalam element parent tersebut

### Fixed 

Elemen akan dicabut / dilepas dari alur dokumen, hampir sama dengan elemen yang diberi properti absolute. Yang menjadi perbedaan, elemen yang diberikan posisi fixed akan selalu mengikuti (relative) dokumen, bukan element parent tertentu, atau dengan scroll halaman web.

![](css-position-696x392.jpg)