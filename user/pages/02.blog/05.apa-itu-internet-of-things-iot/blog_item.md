---
title: 'Apa itu Internet of Things (IoT) ?'
content:
    items: '@self.children'
    limit: 5
    order:
        by: date
        dir: desc
    pagination: true
    url_taxonomy_filters: true
---

Apa itu Internet of Things? Internet of Things adalah segala sesuatu yang terkoneksi dan terhubung dengan Internet. Benda-benda tersebut saling terkoneksi satu sama lain dengan teknologi Internet

Pada hal ini, biasanya sesuatu ini mencakupi benda yang bisa dimanfaatkan dengan menggunakan teknologi Internet, seperti misalnya elektronik (kulkas, lampu, pendingin ruangan), ataupun dapat pula diintegrasikan dengan makhluk hidup (hewan)

### Sejarah Internet of Things

Perkembangan Internet of Things sendiri semakin sini semakin pesat, yang disebabkan oleh teknologi Internet yang semakin cepat dan perangkat keras yang semakin cepat dan efisien dari segi daya

### Contoh Internet of Things

Sebagai contoh, teknologi wearable yang bisa temukan adalah smartwatch, yang biasanya kita temukan di Android Wear, Apple Watch ataupun Pebble.

Smart-band seperti Xiaomi Mi Band dan FitBit sendiri pun memiliki manfaat yang sama, namun difokuskan kepada solusi di bidang kesehatan (fitness)

![](1.jpg)

### Apa itu Nest

Ada pula solusi Internet of Things yang mencakup solusi di dalam rumah, yaitu Home Automation seperti Nest. Nest sendiri merupakan IoT yang mencakup tentang pendeteksian suhu (Thermostat) dan sistem keamanan (Security System)

![](2.jpg)

(red)

<iframe width="560" height="315" src="https://www.youtube.com/embed/Q3ur8wzzhBU" frameborder="0" allowfullscreen></iframe>