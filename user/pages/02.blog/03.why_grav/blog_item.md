---
title: 'Why Grav?'
content:
    items: '@self.children'
    limit: 5
    order:
        by: date
        dir: desc
    pagination: true
    url_taxonomy_filters: true
---

The inspiration behind Grav's development was to create a CMS that fills a gap between simple, static sites and complex multiuser blogs. When the project's founder, Andy Miller, took a look at the flat-file solutions already out there, he found current options were either commercial, underpowered, or both.

This isn't for lack of technology. Twig tempting, YAML configuration, markdown content support, and other exceptionally user-friendly features were out there, but they hadn't been assembled into a single CMS available for free.

We felt it was important to make Grav not only free, but open source. This enables its community of developers to contribute code, have a definite say in the project's development, and to easily create plugins and themes that take full advantage of Grav's capabilities.