---
title: 'Flat-file Structure'
published: true
content:
    items: '@self.children'
    limit: 5
    order:
        by: date
        dir: desc
    pagination: true
    url_taxonomy_filters: true
---

Flat-file Structure

One of Grav's biggest features is its flat-file structure. This enables it to not only break free of the need of a database, but for its content to be simply created, swapped, and edited.

Installing Grav on a server is as simple as downloading the Grav core, or a skeleton which includes demo content to get you started, and unpacking it on your server.

There is no database to set up or installation wizard to work through.
![](S8d0ZL8.png)

User Folder

Grav has an intuitive organizational structure, as well. For example, just about everything that isn't core to Grav is stored in a user folder. This includes any custom configuration and settings, user accounts, themes, site-specific assets, plugins, and content.

At its basic level, backing up a Grav site is as simple as making a copy of the user folder.

The site's content is stored in the user/pages directory. Here, pages are organized using a directory structure. Each blog post, for example, would have its own folder containing a markdown file and any images used in the post. Images can be stored in many different ways, but this is one popular example.![](7x9LS8a.png)
